# ©2022 ETH Zurich, Axel Theorell

import cobra
import pandas as pd
from symbolic_system.specific_systems import (
    ChemostatSystemRC,
    ChemostatSystemRA,
    BatchSystemRC,
)
from utility_classes import ChemicalEnvironment, Consortium
from wl_solver import WLInterface, WLParser
import numpy as np
from scipy.optimize import least_squares
from symbolic_system.system_simplifier import Simplifier
from copy import deepcopy


def make_cai_chan_models(name_list):
    return {name: make_cai_chan_model_simplified(name) for name in name_list}


def make_cai_chan_model_simplified(name):
    met_names = ["Se", "Ae", "Be", "Ac", "Bc", "Sc"]
    metabolites = dict()
    for met in met_names:
        if met.endswith("e"):
            metabolites[met] = cobra.Metabolite(met, compartment="e")
        else:
            metabolites[met] = cobra.Metabolite(met, compartment="c")
    r_names = ["EXS", "tS", "tA", "tB", "rA", "rB", "Mu"]
    r_names = [r + name for r in r_names]
    reactions = {r: cobra.Reaction(r) for r in r_names}

    for reaction in reactions.values():
        reaction.lower_bound = 0
    # EXS is only a test reaction which is removed later
    reactions["EXS" + name].add_metabolites({metabolites["Se"]: 1})

    reactions["tS" + name].upper_bound = 10
    reactions["tA" + name].upper_bound = 10
    reactions["tB" + name].upper_bound = 10
    reactions["tA" + name].lower_bound = -10
    reactions["tB" + name].lower_bound = -10
    reactions["tS" + name].add_metabolites(
        {metabolites["Se"]: -1, metabolites["Sc"]: 1}
    )
    reactions["tA" + name].add_metabolites(
        {metabolites["Ae"]: -1, metabolites["Ac"]: 1}
    )
    reactions["tB" + name].add_metabolites(
        {metabolites["Be"]: -1, metabolites["Bc"]: 1}
    )

    if name.endswith("1"):
        reactions["rA" + name].add_metabolites(
            {metabolites["Sc"]: -2, metabolites["Ac"]: 1}
        )
        reactions["rB" + name].add_metabolites(
            {metabolites["Sc"]: -1, metabolites["Bc"]: 1}
        )
        reactions["Mu" + name].add_metabolites(
            {metabolites["Ac"]: -1, metabolites["Bc"]: -1}
        )
    elif name.endswith("2"):
        reactions["rA" + name].add_metabolites(
            {metabolites["Sc"]: -1, metabolites["Ac"]: 1}
        )
        reactions["rB" + name].add_metabolites(
            {metabolites["Sc"]: -2, metabolites["Bc"]: 1}
        )
        reactions["Mu" + name].add_metabolites(
            {metabolites["Ac"]: -1, metabolites["Bc"]: -1}
        )
    else:
        raise ValueError

    model = cobra.Model(name)
    model.add_metabolites(metabolites.values())
    model.add_reactions(reactions.values())
    model.objective = "Mu" + name
    model.repair()
    res = model.optimize()
    assert res.objective_value > 0
    fva1 = cobra.flux_analysis.flux_variability_analysis(model, fraction_of_optimum=0)
    # remove unneeded upper bounds
    for r in list(model.reactions):
        if r.upper_bound == 1000:
            r.upper_bound = np.inf
    model.repair()
    fva2 = cobra.flux_analysis.flux_variability_analysis(model, fraction_of_optimum=0)
    # remove the test reaction from
    model.remove_reactions(["EXS" + name])
    model.repair()

    return model


def create_Cai_Chan_System(org_name_list, RC=False, batch=False):
    org_dict = make_cai_chan_models(org_name_list)
    uptake_prop = 1
    D = 0.5
    consortium = Consortium(org_dict, uptake_prop=uptake_prop)
    # make a chemical environment
    extracellular = {
        met.id
        for met in list(org_dict.values())[0].metabolites
        if met.compartment == "e"
    }
    C_supply = pd.Series({"Ae": 0, "Be": 0, "Se": 10})
    environment = ChemicalEnvironment(D=D, C_supply=C_supply)
    if RC:
        system = ChemostatSystemRC(consortium, environment)
    elif batch:
        system = BatchSystemRC(consortium, environment)
    else:
        system = ChemostatSystemRA(consortium, environment)
    return system, environment, consortium


if __name__ == "__main__":
    org_names = ["CaiChan1", "CaiChan2"]
    interface = WLInterface()
    swap_dict = {"rA": "rB", "EqAc": "EqBc", "tA": "tB"}
    swap_dict.update({val: key for key, val in swap_dict.items()})
    batchSystemRC, batch_environmentRC, batch_consortiumRC = create_Cai_Chan_System(
        org_names, batch=True
    )
    results_dict_reduced_batch = Simplifier.solve_simplified_system(
        batchSystemRC, batch_consortiumRC, swap_dict, interface
    )
    chemostatSystemRC, environmentRC, consortiumRC = create_Cai_Chan_System(
        org_names, RC=True
    )
    results_dict_reducedRC = Simplifier.solve_simplified_system(
        chemostatSystemRC, consortiumRC, swap_dict, interface
    )
    chemostatSystemRA, environmentRA, consortiumRA = create_Cai_Chan_System(org_names)
    results_dict_reducedRA = Simplifier.solve_simplified_system(
        chemostatSystemRA, consortiumRA, swap_dict, interface
    )
    results_dicts = {
        "CRA": results_dict_reducedRA,
        "CRC": results_dict_reducedRC,
        "SSBRC": results_dict_reduced_batch,
    }
    conversion = {0: "O", 1: "l"}
    for name, results_dict in results_dicts.items():
        for ind, result in enumerate(results_dict["regular_solution"]):
            new_result = dict()
            for key, val in result.items():
                if not "lagrange" in key:
                    new_key = key.replace("CaiChan", "").upper()
                    new_result[new_key] = val
            if not "out_df" in locals():
                out_df = pd.DataFrame(index=new_result.keys())

            out_df[name + conversion[ind]] = pd.Series(new_result)
    out_df.to_csv("CaiChan_result.csv", float_format="%.3g")
    interface.session.terminate()
