# ©2022 ETH Zurich, Axel Theorell

import pandas as pd

sol_1_single = pd.Series(
    {
        "Ae": 0.5,
        "Be": 1.25,
        "XOrganism1": 0.75,
        "vAOrganism1": 1.0,
        "vBOrganism1": 0.5,
        "MuOrganism1": 0.5,
        "lagrangeEqAcOrganism1": -0.5,
        "lagrangeEqBcOrganism1": 0,
        "lagrangeInEqvAOrganism1UB": 0.5,
        "lagrangeInEqvAOrganism1LB": 0,
        "lagrangeInEqvBOrganism1UB": 0,
        "lagrangeInEqvBOrganism1LB": 0,
        "lagrangeInEqMuOrganism1LB": 0,
        "vAOrganism1UBBinary": 0,
        "vAOrganism1LBBinary": 1,
        "vBOrganism1UBBinary": 1,
        "vBOrganism1LBBinary": 1,
        "MuOrganism1LBBinary": 0,
    }
)
sol_2_single = pd.Series(
    {
        "Ae": 2.0,
        "Be": 2.0,
        "XOrganism1": 0,
        "vAOrganism1": 4.0,
        "vBOrganism1": 2.0,
        "MuOrganism1": 2.0,
        "lagrangeEqAcOrganism1": -0.5,
        "lagrangeEqBcOrganism1": 0,
        "lagrangeInEqvAOrganism1UB": 0.5,
        "lagrangeInEqvAOrganism1LB": 0,
        "lagrangeInEqvBOrganism1UB": 0,
        "lagrangeInEqvBOrganism1LB": 0,
        "lagrangeInEqMuOrganism1LB": 0,
        "vAOrganism1UBBinary": 0,
        "vAOrganism1LBBinary": 1,
        "vBOrganism1UBBinary": 1,
        "vBOrganism1LBBinary": 1,
        "MuOrganism1LBBinary": 0,
    }
)
