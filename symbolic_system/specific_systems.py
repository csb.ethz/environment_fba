# ©2022 ETH Zurich, Axel Theorell

from copy import deepcopy
from sympy import symbols
import numpy as np
from symbolic_system.make_nl_system import SystemUtils
from symbolic_system.naming_utils import NamingUtils
from symbolic_system.system_evaluator import SystemEvaluator


class SymbolicSystem:
    pass


class BatchSystemRC(SymbolicSystem):
    def __init__(self, consortium, environment):
        # remove the concentration symbols
        environment.vars = list()

        # make symbols
        self.sym_dict = SystemUtils.make_symbols(environment, consortium)

        # make additional Lagrange multipliers
        self.sym_dict.update(SystemUtils.make_batch_symbols(environment))

        # add growth rate symbol
        self.sym_dict["growthRate"] = symbols("growthRate")

        # make extracellular flux equations, REMEMBER THAT THIS IS AN INEQUALITY
        extracellular_eqs = SystemUtils.make_extracellular_equations(
            environment, consortium, self.sym_dict, batch=True
        )

        # make growth equations
        growth_eqs = SystemUtils.make_growth_equations(
            environment,
            consortium,
            self.sym_dict,
            comm_growth=self.sym_dict["growthRate"],
        )

        # make concentration make organism concentration equation
        org_conc_eq = dict()
        org_conc_eq["org_conc_equation"] = (
            np.sum([self.sym_dict[key] for key in self.sym_dict if key.startswith("X")])
            - 1
        )

        # make stoichiometric constraints
        stoichiometric_eqs = SystemUtils.make_stoichiometric_equations(
            consortium, self.sym_dict
        )

        # make variable bounds
        self.ineq_dict = SystemUtils.make_variable_bounds(
            environment, consortium, self.sym_dict, batch=True
        )

        # make lagrange constraints
        lagrange_eqs = SystemUtils.make_lagrange_flux_eqs(
            consortium, self.sym_dict, batch=True
        )

        # make complementary slackness equations
        bound_dict = self.ineq_dict["flux_bounds"]
        bound_dict.update(extracellular_eqs)
        c_s_eqs = SystemUtils.make_c_s_eqs(self.sym_dict, bound_dict)
        assert len(self.sym_dict) == len(lagrange_eqs) + len(stoichiometric_eqs) + len(
            org_conc_eq
        ) + len(growth_eqs) + len(c_s_eqs)
        self.eq_dict = {
            NamingUtils.LAGRANGE_EQ_NAME: lagrange_eqs,
            "stoichiometric_eqs": stoichiometric_eqs,
            "org_conc_eq": org_conc_eq,
            "growth_eqs": growth_eqs,
            "c_s_eqs": c_s_eqs,
        }

        self.ineq_dict[NamingUtils.EXTRACELLULAR_EQ_NAME] = extracellular_eqs


class ChemostatSystemRC(SymbolicSystem):
    def __init__(self, consortium, environment):

        # make symbols
        self.sym_dict = SystemUtils.make_symbols(environment, consortium)

        # make additional Lagrange multipliers

        self.sym_dict.update(
            SystemUtils.make_chemostat_flux_bound_multipliers(consortium)
        )
        self.sym_dict.update(SystemUtils.make_RC_symbols(environment))

        # make extracellular flux equations
        extracellular_eqs = SystemUtils.make_extracellular_equations(
            environment, consortium, self.sym_dict
        )

        # make growth equations
        growth_eqs = SystemUtils.make_growth_equations(
            environment, consortium, self.sym_dict
        )

        # make stoichiometric constraints
        stoichiometric_eqs = SystemUtils.make_stoichiometric_equations(
            consortium, self.sym_dict
        )

        # make variable bounds
        self.ineq_dict = SystemUtils.make_variable_bounds(
            environment, consortium, self.sym_dict
        )

        # make lagrange flux equations
        lagrange_eqs = SystemUtils.make_lagrange_flux_eqs(
            consortium, self.sym_dict, RC=True
        )

        # make lagrange concentration equations
        lagrange_conc_eqs = SystemUtils.make_lagrange_conc_eqs(
            environment, self.ineq_dict, self.sym_dict
        )
        lagrange_eqs.update(lagrange_conc_eqs)

        # make complementary slackness equations
        bound_dict = self.ineq_dict["flux_bounds"]
        bound_dict.update(
            {
                key: val
                for key, val in self.ineq_dict["concentration_bounds"].items()
                if not key.startswith("X")
            }
        )
        c_s_eqs = SystemUtils.make_c_s_eqs(self.sym_dict, bound_dict)

        # make concentration complementary slackness equations
        # c_s_conc_eqs = SystemUtils.make_c_s_conc_eqs(self.sym_dict, self.ineq_dict["flux_bounds"])

        # check that the number of equations and symbols are equal
        assert len(self.sym_dict) == len(lagrange_eqs) + len(stoichiometric_eqs) + len(
            extracellular_eqs
        ) + len(growth_eqs) + len(c_s_eqs)
        self.eq_dict = {
            NamingUtils.LAGRANGE_EQ_NAME: lagrange_eqs,
            "stoichiometric_eqs": stoichiometric_eqs,
            NamingUtils.EXTRACELLULAR_EQ_NAME: extracellular_eqs,
            "growth_eqs": growth_eqs,
            "c_s_eqs": c_s_eqs,
        }


class ChemostatSystemRA(SymbolicSystem):
    def __init__(self, consortium, environment):

        # make symbols
        self.sym_dict = SystemUtils.make_symbols(environment, consortium)

        # make additional Lagrange multipliers
        self.sym_dict.update(
            SystemUtils.make_chemostat_flux_bound_multipliers(consortium)
        )

        # make extracellular flux equations
        extracellular_eqs = SystemUtils.make_extracellular_equations(
            environment, consortium, self.sym_dict
        )

        # make growth equations
        growth_eqs = SystemUtils.make_growth_equations(
            environment, consortium, self.sym_dict
        )

        # make stoichiometric constraints
        stoichiometric_eqs = SystemUtils.make_stoichiometric_equations(
            consortium, self.sym_dict
        )

        # make variable bounds
        self.ineq_dict = SystemUtils.make_variable_bounds(
            environment, consortium, self.sym_dict
        )

        # make lagrange constraints
        lagrange_eqs = SystemUtils.make_lagrange_flux_eqs(consortium, self.sym_dict)

        # make complementary slackness equations
        c_s_eqs = SystemUtils.make_c_s_eqs(self.sym_dict, self.ineq_dict["flux_bounds"])

        # check that number of equations and symbols are equal
        assert len(self.sym_dict) == len(lagrange_eqs) + len(stoichiometric_eqs) + len(
            extracellular_eqs
        ) + len(growth_eqs) + len(c_s_eqs)
        self.eq_dict = {
            NamingUtils.LAGRANGE_EQ_NAME: lagrange_eqs,
            "stoichiometric_eqs": stoichiometric_eqs,
            NamingUtils.EXTRACELLULAR_EQ_NAME: extracellular_eqs,
            "growth_eqs": growth_eqs,
            "c_s_eqs": c_s_eqs,
        }
        # self.ineq_dict = self.bound_ineqs
        # chemostatSystem = {"eq_dict": eq_dict, "ineq_dict": bound_ineqs, "sym_dict": sym_dict}

    def formulate_lps(self, conc_dict):
        lp_system = deepcopy(self)
        # remove unnecessary equations
        lp_system.eq_dict.pop(NamingUtils.EXTRACELLULAR_EQ_NAME)
        lp_system.eq_dict.pop("growth_eqs")
        lp_system.ineq_dict.pop("concentration_bounds")
        lp_system.eq_dict = SystemEvaluator.eval_eqs(lp_system, conc_dict)
        lp_system.ineq_dict = SystemEvaluator.eval_ineqs(lp_system, conc_dict)
        # remove unnecessary symbols
        to_be_removed = list()
        for key in lp_system.sym_dict:
            if key.startswith("X") or (key in conc_dict):
                to_be_removed.append(key)
        [lp_system.sym_dict.pop(key) for key in to_be_removed]
        return lp_system

    def finite_difference_system(self, conc_dict, stretch):
        sys_dict = dict()
        for key in conc_dict:
            # ensure positive concentrations
            if conc_dict[key] > stretch / 2:
                conc_plus_dict = conc_dict.copy()
                conc_plus_dict[key] += stretch / 2
                conc_minus_dict = conc_dict.copy()
                conc_minus_dict[key] -= stretch / 2
            else:
                conc_plus_dict = conc_dict.copy()
                conc_plus_dict[key] += stretch
                conc_minus_dict = conc_dict.copy()

            sys_dict[key] = (
                self.formulate_lps(conc_minus_dict),
                self.formulate_lps(conc_plus_dict),
            )
        return sys_dict
