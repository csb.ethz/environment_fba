# ©2022 ETH Zurich, Axel Theorell

from collections import defaultdict
import numpy as np
import pandas as pd
from symbolic_system.naming_utils import NamingUtils


class SystemEvaluator:
    @staticmethod
    def inequality_cost(viol_vector):
        viol_vector = viol_vector
        # viol_vector = 1000 * (viol_vector ** 2)
        return viol_vector

    @staticmethod
    def complementary_slackness_cost(viol_vector):
        return viol_vector * 5

    @staticmethod
    def eval_eqs(system, result):
        eval_dict = defaultdict(dict)
        for greater_name, eq_dict in system.eq_dict.items():
            for eq_name, expr in eq_dict.items():
                eval_dict[greater_name][eq_name] = expr.xreplace(result)
        return eval_dict

    @staticmethod
    def eval_ineqs(system, result):
        eval_dict = defaultdict(dict)
        for greater_name, eq_dict in system.ineq_dict.items():
            for eq_name, expr in eq_dict.items():
                eval_dict[greater_name][eq_name] = expr.xreplace(result)
        return eval_dict

    @staticmethod
    def eval(system, result):

        eqs = SystemEvaluator.eval_eqs(system, result)
        ineqs = SystemEvaluator.eval_ineqs(system, result)
        return {"eqs": eqs, "ineqs": ineqs}

    @staticmethod
    def jac_eval(result_array, **kwargs):
        vals = SystemEvaluator.feval(result_array, **kwargs)

        derivatives = kwargs["derivatives"]
        system = kwargs["system"]
        result = {
            system.sym_dict[key]: val
            for key, val in zip(sorted(system.sym_dict.keys()), result_array.tolist())
        }
        # first evaluate all the expressions
        jacobian = derivatives.jacobian.copy()
        for ind, col, expr in derivatives.non_constants:
            jacobian.loc[ind, col] = float(expr.xreplace(result))

        # then nullify all met inequality constraints
        bool_vals = vals == 0
        bool_vals[: derivatives.n_eqs] = False
        jacobian.iloc[bool_vals, :] = 0

        return jacobian.values

    @staticmethod
    def feval(result_array, **kwargs):
        system = kwargs["system"]
        assert isinstance(result_array, np.ndarray)
        result = {
            system.sym_dict[key]: val
            for key, val in zip(sorted(system.sym_dict.keys()), result_array.tolist())
        }
        eqs = SystemEvaluator.eval_eqs(system, result)
        # TODO: do not remake the index in the loop
        eq_index = NamingUtils.make_equation_index_from_system(system)
        eqs_series = pd.Series(np.zeros(len(eq_index)), index=eq_index)
        for sub_key in eqs:
            for key in eqs[sub_key]:
                if sub_key.startswith(NamingUtils.COMPLEMENTARY_SLACKNESS_NAME):
                    eqs_series[
                        NamingUtils.combined_category_eq_name(sub_key, key)
                    ] = SystemEvaluator.complementary_slackness_cost(eqs[sub_key][key])
                else:
                    eqs_series[
                        NamingUtils.combined_category_eq_name(sub_key, key)
                    ] = eqs[sub_key][key]
        ineqs = SystemEvaluator.eval_ineqs(system, result)
        ineq_index = NamingUtils.make_inequality_index_from_system(system)
        ineqs_series = pd.Series(np.zeros(len(ineq_index)), ineq_index)
        for sub_key in ineqs:
            for key in ineqs[sub_key]:
                ineqs_series[
                    NamingUtils.combined_category_eq_name(sub_key, key)
                ] = ineqs[sub_key][key]
        ineqs_series.iloc[:] = SystemEvaluator.inequality_cost(
            np.maximum(ineqs_series.values, 0)
        )
        out = pd.concat([eqs_series, ineqs_series])
        return out.values

    @staticmethod
    def check_all_equations(subs, tol=1e-5):
        bool_series = pd.Series()
        for name in subs["eqs"]:
            for var, val in subs["eqs"][name].items():
                bool_series[name + "_" + var] = np.abs(val) < tol
        for name in subs["ineqs"]:
            for var, val in subs["ineqs"][name].items():
                bool_series[name + "_" + var] = val < tol
        return bool_series

    @staticmethod
    def max_violation(subs):
        bool_series = pd.Series()
        for name in subs["eqs"]:
            for var, val in subs["eqs"][name].items():
                bool_series[name + "_" + var] = np.abs(val)
        for name in subs["ineqs"]:
            for var, val in subs["ineqs"][name].items():
                bool_series[name + "_" + var] = np.max(val, 0)

        return {bool_series[bool_series == bool_series.max()].index[0]: float(bool_series.max())}
