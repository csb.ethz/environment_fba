# ©2022 ETH Zurich, Axel Theorell

from sympy import *
from symbolic_system.naming_utils import NamingUtils

# from optlang import Variable as symbols
from collections import defaultdict
import numpy as np


class SystemUtils:
    @staticmethod
    def make_symbols(environment, consortium):
        sym_dict = dict()
        # make concentration symbols
        for var in environment.vars + consortium.X_vars:
            sym_dict[var] = symbols(var)

        for organism in consortium.flux_vars:
            # make flux symbols
            for var in consortium.flux_vars[organism]:
                sym_dict[var] = symbols(var)
            # make lagrange multipliers for stoichiometric equations
            for met in consortium.organism_dict[organism].metabolites:
                if met.compartment != "e":
                    l_name = NamingUtils.lagrange_eq_mult_name(met.id + organism)
                    sym_dict[l_name] = symbols(l_name)
            # make lagrange multipliers for upper and lower reaction bounds
            for r in consortium.organism_dict[organism].reactions:
                if not np.isinf(r.lower_bound):
                    var_lb = NamingUtils.var_lb_name(r.id)
                    l_name = NamingUtils.lagrange_ineq_mult_name(var_lb)
                    sym_dict[l_name] = symbols(l_name)
        return sym_dict

    @staticmethod
    def make_chemostat_flux_bound_multipliers(consortium):
        sym_dict = dict()
        # make upper bound lagrange multipliers for fluxes
        for organism in consortium.flux_vars:
            for r in consortium.organism_dict[organism].reactions:
                bound_set = False
                for met in r.metabolites:
                    if met.compartment == "e":
                        # check if the metabolite is taken up
                        if r.metabolites[met] < 0:
                            var_ub = NamingUtils.metabolite_specific_ub_name(
                                r.id, met.id
                            )
                            l_name = NamingUtils.lagrange_ineq_mult_name(var_ub)
                            sym_dict[l_name] = symbols(l_name)
                            bound_set = True
                if (not np.isinf(r.upper_bound)) and (not bound_set):
                    var_ub = NamingUtils.var_ub_name(r.id)
                    l_name = NamingUtils.lagrange_ineq_mult_name(var_ub)
                    sym_dict[l_name] = symbols(l_name)

        return sym_dict

    @staticmethod
    def make_batch_symbols(environment, consortium):
        sym_dict = dict()
        # make lagrange multipliers for the extracellular balance equations
        for var in environment.C_supply.index:
            l_name = NamingUtils.lagrange_ineq_mult_name(var)
            sym_dict[l_name] = symbols(l_name)
        # make upper bound lagrange multipliers for fluxes
        for organism in consortium.flux_vars:
            for r in consortium.organism_dict[organism].reactions:
                if not np.isinf(r.upper_bound) or ("e" in r.compartments):
                    var_ub = NamingUtils.var_ub_name(r.id)
                    l_name = NamingUtils.lagrange_ineq_mult_name(var_ub)
                    sym_dict[l_name] = symbols(l_name)
        return sym_dict

    @staticmethod
    def make_RC_symbols(environment):
        sym_dict = dict()
        for var in environment.vars:
            l_name = NamingUtils.lagrange_eq_mult_name(var)
            sym_dict[l_name] = symbols(l_name)
            l_name = NamingUtils.lagrange_ineq_mult_name(NamingUtils.var_lb_name(var))
            sym_dict[l_name] = symbols(l_name)
        return sym_dict

    @staticmethod
    def make_extracellular_equations(environment, consortium, sym_dict, batch=False):
        # link flux and concentration variables
        extr_dict = dict()
        for org_name, org in consortium.organism_dict.items():
            extr_dict[org_name] = defaultdict(set)
            for met in org.metabolites:
                if met.compartment == "e":
                    temp_set = {r for r in met.reactions}
                    extr_dict[org_name][met.id].update(temp_set)

        extracellular_eqs = dict()
        for var in environment.C_supply.index:
            if not batch:
                expr = environment.D * (environment.C_supply[var] - sym_dict[var])
            else:
                expr = environment.C_supply[var]
            # get all organism reactions
            for org, reactions in extr_dict.items():
                for r in reactions[var]:
                    temp_met = consortium.organism_dict[org].metabolites.get_by_id(var)
                    expr += (
                        r.metabolites[temp_met] * sym_dict[NamingUtils.make_biomass_variable_name(org)] * sym_dict[r.id]
                    )
            if batch:
                expr *= -1
            extracellular_eqs[var] = expr
        return extracellular_eqs

    @staticmethod
    def make_growth_equations(environment, consortium, sym_dict, comm_growth=None):
        growth_eqs = dict()
        for org in consortium.organism_dict:
            if comm_growth is None:
                growth_eqs[org] = sym_dict[NamingUtils.make_biomass_variable_name(org)] * (
                    environment.D - sym_dict[NamingUtils.make_growth_reaction_name(org)]
                )
            else:
                growth_eqs[org] = sym_dict[NamingUtils.make_biomass_variable_name(org)] * (
                    comm_growth - sym_dict[NamingUtils.make_growth_reaction_name(org)]
                )
        return growth_eqs

    @staticmethod
    def make_stoichiometric_equations(consortium, sym_dict):
        stoichiometric_eqs = dict()
        for org, model in consortium.organism_dict.items():
            for met in model.metabolites:
                # only constrain intracellular variables
                if (met.id not in sym_dict) and (met.compartment != "e"):
                    expr_list = [
                        r.metabolites[met] * sym_dict[r.id] for r in met.reactions
                    ]
                    stoichiometric_eqs[met.id + org] = np.sum(expr_list)
        return stoichiometric_eqs

    @staticmethod
    def make_variable_bounds(environment, consortium, sym_dict, batch=False):

        # bound the fluxes
        flux_bounds = dict()
        for organism in consortium.flux_vars:
            for r in consortium.organism_dict[organism].reactions:
                bound_name = NamingUtils.var_ub_name(r.id)
                bound_set = False
                if "e" in r.compartments:
                    # find the metabolite that is in the extracellular compartment
                    if not batch:
                        for met in r.metabolites:
                            if met.compartment == "e":
                                # check if the metabolite is taken up
                                if r.metabolites[met] < 0:

                                    flux_bounds[bound_name + "_" + met.id] = (
                                        sym_dict[r.id]
                                        - consortium.uptake_prop * sym_dict[met.id]
                                    )
                                    bound_set = True
                # if the reaction is a pure secretion or we are in batch, make a standard constraint
                #         if not bound_name in flux_bounds:
                #             flux_bounds[bound_name] = sym_dict[r.id]
                #     else:
                #         flux_bounds[bound_name] = (
                #             sym_dict[r.id] - r.upper_bound
                #         )
                if (not np.isinf(r.upper_bound)) and (not bound_set):
                    flux_bounds[bound_name] = sym_dict[r.id] - r.upper_bound
                if not np.isinf(r.lower_bound):
                    bound_name = NamingUtils.var_lb_name(r.id)
                    flux_bounds[bound_name] = -sym_dict[r.id] + r.lower_bound

        # bound the concentrations
        concentration_bounds = dict()
        for var in environment.vars + consortium.X_vars:
            concentration_bounds[NamingUtils.var_lb_name(var)] = -sym_dict[var]

        # bound some lagrange multipliers
        lagrange_bounds = {
            key: -val
            for key, val in sym_dict.items()
            if key.startswith(NamingUtils.LAGRANGE_MULT_INEQ_NAME)
        }

        bound_ineqs = {
            NamingUtils.FLUX_BOUNDS_NAME: flux_bounds,
            NamingUtils.CONCENTRATION_BOUNDS_NAME: concentration_bounds,
            NamingUtils.LAGRANGE_BOUNDS_NAME: lagrange_bounds,
        }
        return bound_ineqs

    @staticmethod
    def make_c_s_eqs(sym_dict, ineqs):
        c_s_eqs = dict()
        for name, sym in sym_dict.items():
            if name.startswith(NamingUtils.LAGRANGE_MULT_INEQ_NAME):
                ineq_name = name.replace(NamingUtils.LAGRANGE_MULT_INEQ_NAME, "")
                ineq = ineqs[ineq_name]
                c_s_eqs[ineq_name] = sym * ineq
        return c_s_eqs

    # @staticmethod
    # def make_c_s_conc_eqs(sym_dict, ineqs):
    #     c_s_eqs = dict()
    #     for name, sym in sym_dict.items():
    #         if name.startswith("lagrangeInEq"):
    #             ineq_name = name.replace("lagrangeInEq", "")
    #             ineq = ineqs[ineq_name]
    #             c_s_eqs[ineq_name] = sym * ineq
    #     return c_s_eqs

    @staticmethod
    def make_lagrange_flux_eqs(consortium, sym_dict, RC=False, batch=False):
        lagrange_eqs = dict()
        for org, model in consortium.organism_dict.items():
            for r in model.reactions:
                if r.id.startswith(NamingUtils.GROWTH_REACTION_PREFIX):
                    if not RC and not batch:
                        prefix = -1
                    else:
                        prefix = -sym_dict[NamingUtils.biomass_name_from_growth_name(r.id)]

                else:
                    prefix = 0
                # there may be multiple upper bounds
                ub_ids = [
                    sym_id
                    for sym_id in sym_dict
                    if sym_id.startswith(
                        NamingUtils.lagrange_ineq_mult_name(
                            NamingUtils.var_ub_name(r.id)
                        )
                    )
                ]
                if len(ub_ids) > 0:
                    if len(ub_ids) > 1:
                        assert (NamingUtils.var_ub_name(r.id)) not in sym_dict
                    ub_expr = np.sum([sym_dict[ub_id] for ub_id in ub_ids])
                else:
                    ub_expr = 0
                    assert np.isinf(r.upper_bound)
                if (
                    NamingUtils.lagrange_ineq_mult_name(NamingUtils.var_lb_name(r.id))
                    in sym_dict
                ):
                    lb_expr = sym_dict[
                        NamingUtils.lagrange_ineq_mult_name(
                            NamingUtils.var_lb_name(r.id)
                        )
                    ]
                else:
                    lb_expr = 0
                    assert np.isinf(r.lower_bound)
                var_list = [
                    sym_dict[NamingUtils.lagrange_eq_mult_name(met.id + org)] * val
                    for met, val in r.metabolites.items()
                    if met.compartment != "e"
                ]
                if RC:
                    extr_var_list = [
                        sym_dict[NamingUtils.lagrange_eq_mult_name(met.id)]
                        * val
                        * sym_dict[NamingUtils.make_biomass_variable_name(org)]
                        for met, val in r.metabolites.items()
                        if met.compartment == "e"
                    ]
                    lagrange_eqs[r.id] = (
                        prefix
                        + np.sum(var_list)
                        + np.sum(extr_var_list)
                        + ub_expr
                        - lb_expr
                    )
                elif batch:
                    extr_var_list = [
                        sym_dict[NamingUtils.lagrange_ineq_mult_name(met.id)]
                        * val
                        * sym_dict[NamingUtils.make_biomass_variable_name(org)]
                        for met, val in r.metabolites.items()
                        if met.compartment == "e"
                    ]
                    lagrange_eqs[r.id] = (
                        prefix
                        + np.sum(var_list)
                        - np.sum(extr_var_list)
                        + ub_expr
                        - lb_expr
                    )
                else:
                    lagrange_eqs[r.id] = prefix + np.sum(var_list) + ub_expr - lb_expr
        return lagrange_eqs

    @staticmethod
    def make_lagrange_conc_eqs(environment, ineq_dict, sym_dict):
        lagrange_conc_eqs = dict()
        for conc in environment.vars:
            conc_sym = sym_dict[conc]
            # THIS IS DONE INEFFICIENTLY
            sym_list = list()
            for name, expr in ineq_dict[NamingUtils.FLUX_BOUNDS_NAME].items():
                if conc_sym in expr.free_symbols:
                    db_dc = diff(expr, conc_sym)
                    multiplier = sym_dict[NamingUtils.lagrange_ineq_mult_name(name)]
                    sym_list.append(db_dc * multiplier)
            sym_list.append(
                -sym_dict[
                    NamingUtils.lagrange_ineq_mult_name(NamingUtils.var_lb_name(conc))
                ]
            )
            # now go for the equalities
            multiplier = sym_dict[NamingUtils.lagrange_eq_mult_name(conc)]
            sym_list.append(-environment.D * multiplier)
            lagrange_conc_eqs[conc] = np.sum(sym_list)
        return lagrange_conc_eqs
