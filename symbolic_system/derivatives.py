# ©2022 ETH Zurich, Axel Theorell

from symbolic_system.specific_systems import SymbolicSystem
import pandas as pd
from sympy.core.numbers import Zero
from symbolic_system.make_nl_system import SystemUtils
from sympy import diff
from symbolic_system.naming_utils import NamingUtils
from symbolic_system.system_evaluator import SystemEvaluator


class SystemDerivatives:
    def __init__(self, system: SymbolicSystem):
        """
        Computes and stores the jacobian of the system
        """
        # make the index
        index = NamingUtils.make_equation_index_from_system(system)
        ineq_index = NamingUtils.make_inequality_index_from_system(system)
        self.n_ineqs = len(ineq_index)
        self.n_eqs = len(index)
        index = index + ineq_index
        columns = list(sorted(system.sym_dict.keys()))
        eq_expr_dict = {
            NamingUtils.combined_category_eq_name(category, key): system.eq_dict[
                category
            ][key]
            for category in sorted(system.eq_dict)
            for key in sorted(system.eq_dict[category])
        }
        # change expression of complementary slackness equations to make them more important
        for key, val in eq_expr_dict.items():
            if key.startswith(NamingUtils.COMPLEMENTARY_SLACKNESS_NAME):
                eq_expr_dict[key] = SystemEvaluator.complementary_slackness_cost(val)
        ineq_expr_dict = {
            NamingUtils.combined_category_eq_name(
                category, key
            ): SystemEvaluator.inequality_cost(system.ineq_dict[category][key])
            for category in sorted(system.ineq_dict)
            for key in sorted(system.ineq_dict[category])
        }
        eq_expr_dict.update(ineq_expr_dict)
        jacobian = pd.DataFrame(0.0, index=index, columns=columns)
        non_constants = list()
        # now simply loop through columns and rows
        for ind in jacobian.index:
            for col in jacobian.columns:

                sym = system.sym_dict[col]
                eq_expr = eq_expr_dict[ind]
                if sym in eq_expr.free_symbols:
                    diff_expr = diff(eq_expr, sym)
                    if len(diff_expr.free_symbols) > 0:
                        non_constants.append([ind, col, diff_expr])
                    else:
                        jacobian.loc[ind, col] = float(diff_expr)

        self.jacobian = jacobian
        self.non_constants = non_constants
        pass
