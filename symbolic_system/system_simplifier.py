# ©2022 ETH Zurich, Axel Theorell

from copy import deepcopy
from wl_solver import WLParser, WLInterface
from symbolic_system.system_evaluator import SystemEvaluator
import numpy as np
from collections import defaultdict

# swap_dict = {"vA": "vB", "vB": "vA", "EqAc": "EqBc", "EqBc": "EqAc"}


class Simplifier:
    @staticmethod
    def simplify_system(original_system, consortium, swap_dict):
        system = deepcopy(original_system)
        pairs = dict()
        assert len(consortium.organism_dict) == 2
        first_org = list(sorted(consortium.organism_dict.keys()))[0]
        second_org = list(sorted(consortium.organism_dict.keys()))[1]

        for name, var in system.sym_dict.items():
            # for r in r_list:
            if first_org in name:
                for key in swap_dict:
                    if key in name:
                        pairs[var] = system.sym_dict[
                            name.replace(first_org, second_org).replace(
                                key, swap_dict[key]
                            )
                        ]
                        break
                    pairs[var] = system.sym_dict[name.replace(first_org, second_org)]
        # now substitute everything into the system
        expr_dicts = ["eq_dict", "ineq_dict"]
        for outer_expr_dict in expr_dicts:
            existing_exprs = set()
            for expr_dict in getattr(system, outer_expr_dict).values():
                pop_list = list()
                for name, expr in expr_dict.items():
                    subs = expr.subs(pairs)
                    if subs in existing_exprs:
                        pop_list.append(name)
                    else:
                        expr_dict[name] = subs
                        existing_exprs.add(subs)
                for el in pop_list:
                    expr_dict.pop(el)
        # remove unused symbols
        for key in pairs:
            system.sym_dict.pop(key.__str__())
        return system, pairs

    @staticmethod
    def solve_simplified_system(original_system, consortium, swap_dict, interface):
        system, pairs = Simplifier.simplify_system(
            original_system, consortium, swap_dict
        )
        reduced_expr = WLInterface.build_wl_system(system)
        reduced_result = interface.solve_wl_system(reduced_expr)
        results_dict = WLParser.parse_output(reduced_result, separate_solutions=True)
        full_result = Simplifier.check_solution(original_system, pairs, results_dict)
        # convert keys to string
        full_result_str = defaultdict(list)
        for sol_name in full_result:
            for el in full_result[sol_name]:
                temp_dict = dict()
                for key, val in el.items():
                    temp_dict[key.__str__()] = val
                full_result_str[sol_name].append(temp_dict)
        return full_result_str

    @staticmethod
    def check_solution(original_system, pairs, results_dict, tol=1e-12):
        # produce backward pair list
        # rev_pairs = {val: key for key, val in pairs.items()}
        full_result = defaultdict(list)
        for name, result_list in results_dict.items():
            for result in result_list:
                res_dict = dict()
                for sym in original_system.sym_dict.values():
                    if sym.__str__() in result:
                        res_dict[sym] = result[sym.__str__()]
                    elif sym in pairs:
                        res_dict[sym] = result[pairs[sym].__str__()]
                    else:
                        res_dict[sym] = 0
                sys_sol = SystemEvaluator.eval(original_system, res_dict)
                for eq_dict in sys_sol["eqs"].values():
                    for val in eq_dict.values():
                        try:
                            assert np.abs(val) <= tol
                        except TypeError:
                            print(val)
                for ineq_dict in sys_sol["ineqs"].values():
                    for val in ineq_dict.values():
                        try:
                            assert val <= tol
                        except TypeError:
                            print(val)
                full_result[name].append(res_dict)
        return full_result
