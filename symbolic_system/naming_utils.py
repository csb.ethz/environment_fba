# ©2022 ETH Zurich, Axel Theorell

class NamingUtils:
    LAGRANGE_NAME = "lagrange"
    LAGRANGE_MULT_INEQ_NAME = LAGRANGE_NAME + "InEq"
    LAGRANGE_MULT_EQ_NAME = LAGRANGE_NAME + "Eq"
    LAGRANGE_EQ_NAME = "lagrange_eqs"
    EXTRACELLULAR_EQ_NAME = "extracellular_eqs"
    FLUX_BOUNDS_NAME = "flux_bounds"
    CONCENTRATION_BOUNDS_NAME = "concentration_bounds"
    LAGRANGE_BOUNDS_NAME = "lagrange_bounds"
    COMPLEMENTARY_SLACKNESS_NAME = "c_s_eqs"
    GROWTH_EQ_NAME = "growth_eqs"
    BIOMASS_VARIABLE_PREFIX = "X_"
    GROWTH_REACTION_PREFIX = "Mu_"

    @staticmethod
    def biomass_name_from_growth_name(growth_name):
        assert growth_name.startswith(NamingUtils.GROWTH_REACTION_PREFIX)
        return growth_name.replace(NamingUtils.GROWTH_REACTION_PREFIX, NamingUtils.BIOMASS_VARIABLE_PREFIX)

    @staticmethod
    def growth_name_from_biomass_name(growth_name):
        assert growth_name.startswith(NamingUtils.BIOMASS_VARIABLE_PREFIX)
        return growth_name.replace(NamingUtils.BIOMASS_VARIABLE_PREFIX, NamingUtils.GROWTH_REACTION_PREFIX)

    @staticmethod
    def make_growth_reaction_name(org_name):
        return NamingUtils.GROWTH_REACTION_PREFIX + org_name

    @staticmethod
    def make_biomass_variable_name(org_name):
        return NamingUtils.BIOMASS_VARIABLE_PREFIX + org_name

    @staticmethod
    def metabolite_specific_ub_name(r_id, met_id):
        return r_id + "UB" + "_" + met_id

    @staticmethod
    def var_ub_name(var: str):
        if var.endswith("UB"):
            return var
        return var + "UB"

    @staticmethod
    def var_lb_name(var: str):
        if var.endswith("LB"):
            return var
        return var + "LB"

    @staticmethod
    def lagrange_eq_mult_name(var):
        return NamingUtils.LAGRANGE_MULT_EQ_NAME + var

    @staticmethod
    def lagrange_ineq_mult_name(var):
        return NamingUtils.LAGRANGE_MULT_INEQ_NAME + var

    @staticmethod
    def combined_category_eq_name(category, eq_name):
        return category + "_" + eq_name

    @staticmethod
    def make_equation_index_from_system(system):
        return sorted(
            [
                NamingUtils.combined_category_eq_name(category, key)
                for category in system.eq_dict
                for key in system.eq_dict[category]
            ]
        )

    @staticmethod
    def make_inequality_index_from_system(system):
        return sorted(
            [
                NamingUtils.combined_category_eq_name(category, key)
                for category in system.ineq_dict
                for key in system.ineq_dict[category]
            ]
        )
    @staticmethod
    def remove_model_suffix(var_name, model_names):
        for name in model_names:
            var_name = var_name.replace(name, "")
        var_name = var_name.strip("_")
        return var_name
