# ©2022 ETH Zurich, Axel Theorell

from wolframclient.evaluation import WolframLanguageSession
from wolframclient.language import wl, wlexpr
from wolframclient.language.expression import WLFunction
from collections import defaultdict
import numpy as np
import pandas as pd


class WLParser:
    @staticmethod
    def parse_output(results, separate_solutions=True):
        results_list = list()
        for ind, sol in enumerate(results):
            results_list.append(dict())
            for fct in sol:
                assert fct.args[0].name.startswith("Global")
                if hasattr(fct.args[1], "head"):
                    if fct.args[1].head.name == "Rational":
                        results_list[ind][fct.args[0].name.split("`")[1]] = (
                            fct.args[1].args[0] / fct.args[1].args[1]
                        )
                    else:
                        results_list[ind][fct.args[0].name.split("`")[1]] = fct.args[1]
                else:
                    results_list[ind][fct.args[0].name.split("`")[1]] = fct.args[1]
        if separate_solutions:
            return WLParser.make_results_dict(results_list)
        else:
            return results_list

    @staticmethod
    def make_results_dict(results_list):
        results_dict = defaultdict(list)
        for result in results_list:
            zero_solutions = 0
            for key, val in result.items():
                if key.startswith("X"):
                    if val == 0:
                        zero_solutions += 1
                    elif isinstance(val, WLFunction):
                        if val.args[0] == 0:
                            zero_solutions += 1
            if zero_solutions > 0:
                results_dict["zero_solution_" + str(zero_solutions)].append(result)
            else:
                results_dict["regular_solution"].append(result)
        return results_dict


class WLInterface:
    def __init__(self):
        self.session = WolframLanguageSession()

    @staticmethod
    def build_wl_system(chemostatSystem):
        eq_string = " && ".join(
            [
                value.__str__() + " == 0"
                for subdict in chemostatSystem.eq_dict.values()
                for value in subdict.values()
            ]
        )
        ineq_string = " && ".join(
            [
                value.__str__() + " <= 0"
                for subdict in chemostatSystem.ineq_dict.values()
                for value in subdict.values()
            ]
        )
        variables = ", ".join(
            [value.__str__() for value in chemostatSystem.sym_dict.values()]
        )
        # flattened_list = [value for sublist in nested_list for value in sublist]
        # wl_expr = wlexpr("Solve[ x + y == 7 &&  x - y == 1, {x, y}]")
        wl_string = (
            "Solve[" + eq_string + " && " + ineq_string + ", {" + variables + "}" + "]"
        )
        # wl_string = (
        #         "Solve[" + eq_string + ", {" + variables + "}" + "]"
        # )
        # wl_string = wl_string.replace("_", "")
        wl_expr = wlexpr(wl_string)
        return wl_expr

    def solve_wl_system(self, wl_expr):
        result = self.session.evaluate(wl_expr)
        # self.session.terminate()
        return result

    def build_and_solve_system(self, chemostatSystem):
        wl_expr = self.build_wl_system(chemostatSystem)
        return self.solve_wl_system(wl_expr)

    def solve_finite_difference_system(self, sys_dict, stretch):
        jacobian = pd.DataFrame(columns=sys_dict.keys())
        for key in sys_dict:
            solutions = list()
            for ind, sys in enumerate(sys_dict[key]):
                sol = self.build_and_solve_system(sys)
                assert len(sol) == 1
                parsed = WLParser.parse_output(sol, separate_solutions=False)[0]
                parsed = pd.Series(parsed)
                solutions.append(parsed)
            assert len(solutions) == 2
            f_diff = (solutions[1] - solutions[0]) / stretch
            jacobian[key] = f_diff
        return jacobian
