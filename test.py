# ©2022 ETH Zurich, Axel Theorell

import unittest
from minimal_model import minimal_stoichiometric_model, minimal_crossfeeding
from utility_classes import ChemicalEnvironment, Consortium
import pandas as pd
from symbolic_system.specific_systems import (
    BatchSystemRC,
    ChemostatSystemRC,
    ChemostatSystemRA,
)
# from wl_solver import WLInterface, WLParser
import numpy as np

from symbolic_system.system_evaluator import SystemEvaluator
# from symbolic_system.system_simplifier import Simplifier
from numeric_solution.gurobi_interface import GurobiInterface
from test_ground_truths import sol_1_single, sol_2_single
import cobra
from sympy.solvers.solveset import linsolve
from e_coli_core_ensemble_construction import (
    full_initialization,
    get_transport_reactions,
    initialize_single_model,
    system_from_models,
    make_mutually_dependent_community,
)
from numeric_solution.scipy_root_solver import f_jacobian_wrapped, least_squares_solve
from symbolic_system.derivatives import SystemDerivatives
import os

class NumericalSolveTester(unittest.TestCase):

    # def test_num_solve_scip(self):
    #     org_names = ["Organism1"]
    #     chemostatSystem, environment, consortium = Tester.create_min_env(org_names)
    #     solution = solve_system_with_scip(chemostatSystem)
    #     self.assertTrue(np.sum(np.abs(solution - sol_1_single)) < 1e-8)

    # now test a minimal consortium
    # org_names = ["Organism1", "Organism2"]
    # chemostatSystem, environment, consortium = Tester.create_min_env(org_names)
    # solution = solve_system_with_scip(chemostatSystem)
    # pass

    def test_num_solve(self):
        # tell gurobi to find ALL solutions
        settings = {"PoolSearchMode": 2}  # , 'Presolve': 0}
        # first  test a single model
        org_names = ["Organism1"]
        chemostatSystem, environment, consortium = Tester.create_min_env(org_names)
        solutions = GurobiInterface.solve_system_with_gurobi(
            chemostatSystem, settings=settings
        )
        # for now, substitute into original system and see whats wrong

        self.assertEqual(len(solutions), 2)
        # result = {chemostatSystem.sym_dict[key]: val for key, val in solutions[2].iteritems() if key in chemostatSystem.sym_dict}
        # subs = SystemEvaluator.eval(chemostatSystem, result)
        self.assertTrue(np.sum(np.abs(solutions[0] - sol_1_single)) < 1e-8)
        self.assertTrue(np.sum(np.abs(solutions[1] - sol_2_single)) < 1e-8)

        # now test a minimal consortium
        org_names = ["Organism1", "Organism2"]
        chemostatSystem, environment, consortium = Tester.create_min_env(org_names)
        solutions = GurobiInterface.solve_system_with_gurobi(
            chemostatSystem, settings=settings
        )
        self.assertEqual(len(solutions), 4)

    def test_spiralus(self):
        model = cobra.io.read_sbml_model(
            os.path.join("models","thermodynamic_spiral_sbml3.xml")
        )
        bm = model.reactions.v
        # model.objective = {bm: 1}
        # model.objective.direction = 'max'
        sol = model.optimize()
        print(model.summary(sol))

        org_dict = {"model": model}

        model.remove_reactions([bm])
        bm.id = "Mu_model"
        model.add_reactions([bm])
        model.repair()
        uptake_prop = 1
        D = 0.1

        # Create extracellular pool variables
        # active_extracellular = {r.replace("EX_", "") for r, val in gluc_sol.fluxes.iteritems() if r.startswith("EX") and np.abs(val) > 0}.union({r for r, val in ac_sol.fluxes.iteritems() if r.startswith("EX") and np.abs(val) > 0})
        active_extracellular = {m.id for m in model.metabolites if m.compartment == "e"}
        model.remove_reactions(list(model.exchanges))
        model.repair()
        C_supply = pd.Series({r: 10 for r in active_extracellular})
        consortium = Consortium(org_dict, uptake_prop=uptake_prop)
        environment = ChemicalEnvironment(D=D, C_supply=C_supply)
        system = ChemostatSystemRA(consortium, environment)

        sol = GurobiInterface.solve_system_with_gurobi(system, partial_start=dict())
        pass

    def test_crossfeeding(self):
        # tell gurobi to find ALL solutions
        # settings = {'PoolSearchMode': 2}
        org_dict = minimal_crossfeeding()
        extracellular = {
            met.id
            for met in list(org_dict.values())[0].metabolites
            if met.compartment == "e"
        }
        consortium = Consortium(org_dict, uptake_prop=1)
        environment = ChemicalEnvironment(
            D=0.1, C_supply=pd.Series([1, 0], index=sorted(extracellular))
        )
        chemostatSystem = ChemostatSystemRC(consortium, environment)
        solutions = GurobiInterface.solve_system_with_gurobi(chemostatSystem)
        # find the first solution for which both organisms have non-zero concentration
        i = 0
        while i < len(solutions):
            if solutions[i]["X_Organism1"] == 0 or solutions[i]["X_Organism2"] == 0:
                i += 1
            else:
                break
        self.assertAlmostEqual(solutions[i]["Ae"], 0.3, delta=1e-5)
        self.assertAlmostEqual(solutions[i]["Be"], 0.2, delta=1e-5)
        self.assertAlmostEqual(solutions[i]["X_Organism1"], 0.3, delta=1e-5)
        self.assertAlmostEqual(solutions[i]["X_Organism2"], 0.2, delta=1e-5)

    def test_root_finding(self):
        org_names = ["Organism1", "Organism2"]
        system, environment, consortium = Tester.create_min_env(org_names)
        settings = {
            "IntFeasTol": 1e-7,
        }
        model = GurobiInterface.construct_gurobi_model(system, settings=settings)
        model.setParam("TimeLimit", 1)
        # flux_variables = {"X_gluc_model", "X_ac_model"}
        GurobiInterface.relax_eqs(model)
        vars = model.getVars()
        # set relaxation to zero
        # rel_start = {var.VarName: 0 for var in vars if 'relax' in var.VarName}
        force_fluxes = True
        space = np.linspace(0, 10, 3)
        grid = np.meshgrid(space, space)
        derivatives = SystemDerivatives(system)
        for i in range(grid[0].size):

            partial_start = {
                "X_Organism1": grid[0].flatten()[i],
                "X_Organism2": grid[1].flatten()[i],
            }
            # partial_start.update(rel_start)
            GurobiInterface.set_starting_point(model, partial_start, force_fluxes)
            model.optimize()
            subs = {
                system.sym_dict[var.VarName]: var.X
                for var in vars
                if var.VarName in system.sym_dict
            }
            substituted_MILP = SystemEvaluator.eval(system, subs)
            violation_MILP = SystemEvaluator.max_violation(substituted_MILP)

            res = least_squares_solve(system, vars, derivatives)
            x_least_squares = pd.Series(res.x, index=sorted(system.sym_dict.keys()))
            subs = {
                system.sym_dict[key]: val for key, val in x_least_squares.iteritems()
            }
            substituted_least_squares = SystemEvaluator.eval(system, subs)
            violation_least_squares = SystemEvaluator.max_violation(
                substituted_least_squares
            )
            self.assertTrue(list(violation_least_squares.values())[0] <= list(violation_MILP.values())[0])
            # self.assertLess(violation_least_squares, 1e-3)
            # if (x_least_squares['X_Organism1'] > 1e-6 and np.abs((x_least_squares['Mu_Organism1'] - environment.D)) < 1e-6) or
            pass

    # def test_batch_crossfeeding(self):
    #     org_dict = minimal_crossfeeding()
    #     # make a chemical environment
    #     extracellular = {
    #         met.id
    #         for met in list(org_dict.values())[0].metabolites
    #         if met.compartment == "e"
    #     }
    #     consortium = Consortium(org_dict, uptake_prop=1)
    #     environment = ChemicalEnvironment(
    #         D=0.1, C_supply=pd.Series([1, 0], index=sorted(extracellular))
    #     )
    #     batchSystem = BatchSystemRC(consortium, environment)
    #     solutions = solve_system_with_gurobi(batchSystem)
    #     self.assertAlmostEqual(solutions[0]["Ae"], 0.3)
    #     self.assertAlmostEqual(solutions[0]["Be"], 0.2)
    #     self.assertAlmostEqual(solutions[0]["X_Organism1"], 0.3)
    #     self.assertAlmostEqual(solutions[0]["X_Organism2"], 0.2)


class TestEColiCore(unittest.TestCase):
    test_tol = 1e-6
    def e_coli_zero_solution(self, system, C_supply, fluxes):
        syms = system.sym_dict.copy()
        extr_concentration = {syms[key]: val for key, val in C_supply.iteritems()}
        [syms.pop(key.name) for key in extr_concentration]
        fluxes_and_b_mass = {
            syms[key]: fluxes[key.split("_gluc_model")[0]]
            for key in syms
            if key.split("_gluc_model")[0] in fluxes.index
        }
        fluxes_and_b_mass[syms["X_gluc_model"]] = 0
        fluxes_and_b_mass[syms["Mu_gluc_model"]] = fluxes["Mu_gluc_model"]
        [syms.pop(key.name) for key in fluxes_and_b_mass]

        result = extr_concentration.copy()
        result.update(fluxes_and_b_mass)

        substituted = SystemEvaluator.eval_eqs(system, result)
        # test for inconsistency
        eqs = [eq for eq in substituted["c_s_eqs"].values()] + [
            eq for eq in substituted["lagrange_eqs"].values()
        ]
        la_syms = [sym for sym in syms.values()]
        res = linsolve(eqs, la_syms)
        self.assertTrue(len(res) > 0)
        result = {sym.name: val for sym, val in result.items()}
        return result

    def test_single_amino_acid_model(self):
        system_type = "CA"
        system = make_mutually_dependent_community(system_type=system_type, only_ala=True)
        settings = {
            "PoolSearchMode": 0,
            "PoolSolutions": 1,
            "FeasibilityTol": 1e-9,
            "TimeLimit": 2000,
            "OptimalityTol": 1e-5,
            "IntFeasTol": 1e-9,
        }
        sol = GurobiInterface.solve_system_with_gurobi(
            system, settings=settings
        )
        community_sol_dict = {
            el.split("_gluc_model")[0]: [sol[i][el] for i in range(len(sol))]
            for el, val in sol[0].iteritems()
            if el.endswith("_gluc_model")
               and not (el.split("_gluc_model")[0] in ["X", "Mu"])
        }
        pass


    def test_simplified_e_coli_core(self):
        for system_type in ["CC", "CA"]:
            exc_conc = 100
            removal_list = ["PIt2r"]
            org_dict, sol_dict, system, C_supply = full_initialization(
                removal_list, exc_conc, system_type=system_type
            )
            # self.assertTrue(isinstance(system, ChemostatSystemRC))
            test_object = TestEColiCore()
            partial_start = test_object.e_coli_zero_solution(
                system, C_supply, sol_dict["gluc_model"].fluxes
            )
            # partial_start = {"Xgluc_model": 0}
            # In this part, we lock the fluxes to the zero solution and test some properties
            settings = {"PoolSearchMode": 0, "PoolSolutions": 1, "IntFeasTol": 1e-8, "FeasibilityTol": 1e-8,}
            sol = GurobiInterface.solve_system_with_gurobi(
                system, partial_start=partial_start, settings=settings, force_fluxes=True
            )
            community_sol_dict = {
                el.split("_gluc_model")[0]: [sol[i][el] for i in range(len(sol))]
                for el, val in sol[0].iteritems()
                if el.endswith("_gluc_model")
                and not (el.split("_gluc_model")[0] in ["X", "Mu"])
            }
            compare_df = pd.DataFrame.from_dict(community_sol_dict, orient="index")
            compare_df.loc[:, len(compare_df.columns)] = sol_dict["gluc_model"].fluxes[
                compare_df.index
            ]
            diff_df = compare_df.subtract(compare_df[len(compare_df.columns) - 1], axis=0)
            zero_rows = (diff_df.iloc[:, :-1].abs() < TestEColiCore.test_tol).any(axis=1)
            match = (diff_df < TestEColiCore.test_tol).all(axis=0)
            self.assertTrue(zero_rows.all())
            self.assertTrue(match.all())
            # now we try to find a non-zero solution
            sol = GurobiInterface.solve_system_with_gurobi(
                system, partial_start=partial_start, settings=settings, force_fluxes=False
            )
            # the growth rate should equal the dilution rate D=1
            community_sol_dict_full = {
                system.sym_dict[el]: val
                for el, val in sol[0].iteritems()
                if el in system.sym_dict
            }
            substituted = SystemEvaluator.eval(system, community_sol_dict_full)
            bools = SystemEvaluator.check_all_equations(substituted)
            self.assertTrue(bools.all())

    def test_single_e_coli_core(self):
        for system_type in ["CC", "CA"]:
            # now test the whole network
            settings = {
                "PoolSearchMode": 0,
                "PoolSolutions": 1,
                "FeasibilityTol": 1e-7,
                "MIPFocus": 1,
                "OptimalityTol": 1e-6,
                "IntFeasTol": 1e-8,
            }
            exc_conc = 20
            removal_list = []
            alt_org_dict, alt_sol_dict, alt_system, alt_C_supply = full_initialization(
                removal_list, exc_conc, no_removal=True, system_type=system_type
            )
            # self.assertTrue(isinstance(alt_system, ChemostatSystemRC))
            # alt_partial_start = test_e_coli_zero_solution(alt_system, alt_C_supply, alt_sol_dict['gluc_model'].fluxes)
            alt_partial_start = {"X_gluc_model": 0}
            sol2 = GurobiInterface.solve_system_with_gurobi(
                alt_system,
                partial_start=alt_partial_start,
                settings=settings,
                force_fluxes=True,
                omega=1e4,
            )

            community_sol_dict_full = {
                alt_system.sym_dict[el]: val
                for el, val in sol2[0].iteritems()
                if el in alt_system.sym_dict
            }
            alt_substituted = SystemEvaluator.eval(alt_system, community_sol_dict_full)
            bools = SystemEvaluator.check_all_equations(alt_substituted)
            # make sure all equations really pass
            self.assertTrue(bools.all())

    def test_pair_model(self):

        for system_type in ["CC", "CA"]:
            exc_conc = 20
            gluc_model = initialize_single_model(exc_conc)
            gluc_transporters = get_transport_reactions(gluc_model)
            ac_model = initialize_single_model(exc_conc)
            ac_transporters = get_transport_reactions(ac_model)
            org_dict = {"gluc_model": gluc_model, "ac_model": ac_model}
            sol_dict = {"gluc_model": {}, "ac_model": {}}
            system, C_supply = system_from_models(org_dict, exc_conc, system_type=system_type)
            # self.assertTrue(isinstance(system, ChemostatSystemRC))
            partial_start = {"X_gluc_model": 0, "X_ac_model": 0}
            settings = {
                "PoolSearchMode": 1,
                "PoolSolutions": 1,
                "FeasibilityTol": 1e-7,
                "MIPFocus": 1,
                "OptimalityTol": 1e-6,
                "IntFeasTol": 1e-7,
            }
            sol = GurobiInterface.solve_system_with_gurobi(
                system,
                partial_start=partial_start,
                force_fluxes=True,
                settings=settings,
            )
            community_sol_dict_full = {
                system.sym_dict[el]: val
                for el, val in sol[0].iteritems()
                if el in system.sym_dict
            }
            substituted = SystemEvaluator.eval(system, community_sol_dict_full)
            bools = SystemEvaluator.check_all_equations(substituted, tol=1e-4)
            self.assertTrue(bools.all())

class TestAnalyticalJacobian(unittest.TestCase):
    def test_small_analytical_jacobian(self):
        org_names = ["Organism1", "Organism2"]
        system, environment, consortium = Tester.create_min_env(org_names)
        settings = {
            "IntFeasTol": 1e-7,
        }
        model = GurobiInterface.construct_gurobi_model(system, settings=settings)
        model.setParam("TimeLimit", 1)
        # flux_variables = {"X_gluc_model", "X_ac_model"}
        GurobiInterface.relax_eqs(model)
        vars = model.getVars()
        # set relaxation to zero
        # rel_start = {var.VarName: 0 for var in vars if 'relax' in var.VarName}
        force_fluxes = True
        space = np.linspace(0, 10, 3)
        grid = np.meshgrid(space, space)
        derivatives = SystemDerivatives(system)
        for i in range(grid[0].size):

            partial_start = {
                "X_Organism1": grid[0].flatten()[i],
                "X_Organism2": grid[1].flatten()[i],
            }
            # partial_start.update(rel_start)
            GurobiInterface.set_starting_point(model, partial_start, force_fluxes)
            model.optimize()
            num_subs = {
                var.VarName: var.X for var in vars if var.VarName in system.sym_dict
            }

            x0 = np.array([val for key, val in sorted(num_subs.items())])
            x0 += np.random.normal(size=x0.size, scale=0.1)
            num_jac = f_jacobian_wrapped(x0, system)
            anal_jac = SystemEvaluator.jac_eval(
                x0, system=system, derivatives=derivatives
            )
            viol = SystemEvaluator.feval(x0, system=system)
            diff = np.abs(num_jac) - np.abs(anal_jac)
            self.assertTrue(np.max(np.abs(diff)) < 1e-6)
            pass

    def test_small_CC_analytical_jacobian(self):
        org_dict = minimal_crossfeeding()
        extracellular = {
            met.id
            for met in list(org_dict.values())[0].metabolites
            if met.compartment == "e"
        }
        consortium = Consortium(org_dict, uptake_prop=1)
        environment = ChemicalEnvironment(
            D=0.1, C_supply=pd.Series([1, 0], index=sorted(extracellular))
        )
        system = ChemostatSystemRC(consortium, environment)
        settings = {
            "IntFeasTol": 1e-7,
        }
        model = GurobiInterface.construct_gurobi_model(system, settings=settings)
        model.setParam("TimeLimit", 1)
        # flux_variables = {"X_gluc_model", "X_ac_model"}
        GurobiInterface.relax_eqs(model)
        vars = model.getVars()
        # set relaxation to zero
        # rel_start = {var.VarName: 0 for var in vars if 'relax' in var.VarName}
        force_fluxes = True
        space = np.linspace(0, 10, 3)
        grid = np.meshgrid(space, space)
        derivatives = SystemDerivatives(system)
        for i in range(grid[0].size):

            partial_start = {
                "X_Organism1": grid[0].flatten()[i],
                "X_Organism2": grid[1].flatten()[i],
            }
            # partial_start.update(rel_start)
            GurobiInterface.set_starting_point(model, partial_start, force_fluxes)
            model.optimize()
            num_subs = {
                var.VarName: var.X for var in vars if var.VarName in system.sym_dict
            }

            x0 = np.array([val for key, val in sorted(num_subs.items())])
            x0 += np.random.normal(size=x0.size, scale=0.1)
            num_jac = f_jacobian_wrapped(x0, system)
            anal_jac = SystemEvaluator.jac_eval(
                x0, system=system, derivatives=derivatives
            )
            viol = SystemEvaluator.feval(x0, system=system)
            diff = np.abs(num_jac) - np.abs(anal_jac)
            self.assertTrue(np.max(np.abs(diff)) < 1e-6)
            pass
    #
    # def test_large_analytical_jacobian(self):
    #     exc_conc = 20
    #     gluc_model = initialize_single_model(exc_conc)
    #     gluc_transporters = get_transport_reactions(gluc_model)
    #     org_dict = {"gluc_model": gluc_model}
    #     for model in org_dict.values():
    #         bound_single_model(model)
    #     sol_dict = {"gluc_model": {}}
    #
    #     system, C_supply = system_from_models(org_dict, exc_conc)
    #     # bounds = make_variable_bounds_vector(system)
    #     settings = {
    #         "PoolSearchMode": 0,
    #         "PoolSolutions": 1,
    #         "FeasibilityTol": 1e-5,
    #         # "MIPFocus": 1,
    #         "OptimalityTol": 1e-2,
    #         "IntFeasTol": 1e-7,
    #     }
    #
    #     model = GurobiInterface.construct_gurobi_model(system, settings=settings)
    #     model.setParam("TimeLimit", 1)
    #     # flux_variables = {"X_gluc_model", "X_ac_model"}
    #     GurobiInterface.relax_eqs(model)
    #     vars = model.getVars()
    #     # set relaxation to zero
    #     # rel_start = {var.VarName: 0 for var in vars if 'relax' in var.VarName}
    #     force_fluxes = True
    #     space = np.linspace(0, 2, 3)
    #     derivatives = SystemDerivatives(system)
    #     for num in space:
    #         partial_start = {"X_gluc_model": num}
    #         # partial_start.update(rel_start)
    #         GurobiInterface.set_starting_point(model, partial_start, force_fluxes)
    #         model.optimize()
    #         num_subs = {
    #             var.VarName: var.X for var in vars if var.VarName in system.sym_dict
    #         }
    #
    #         x0 = np.array([val for key, val in sorted(num_subs.items())])
    #         x0 += np.random.normal(size=x0.size, scale=0.1)
    #         anal_jac = SystemEvaluator.jac_eval(
    #             x0, system=system, derivatives=derivatives
    #         )
    #         num_jac = f_jacobian_wrapped(x0, system)
    #
    #         viol = SystemEvaluator.feval(x0, system=system)
    #         diff = np.abs(num_jac) - np.abs(anal_jac)
    #         self.assertTrue(np.max(np.abs(diff)) < 1e-6)

    def test_large_CC_analytical_jacobian(self):
        exc_conc = 20
        gluc_model = initialize_single_model(exc_conc)
        gluc_transporters = get_transport_reactions(gluc_model)
        org_dict = {"gluc_model": gluc_model}
        # for model in org_dict.values():
        #     bound_single_model(model)
        sol_dict = {"gluc_model": {}}

        system, C_supply = system_from_models(org_dict, exc_conc, system_type="CC")
        # bounds = make_variable_bounds_vector(system)
        settings = {
            "PoolSearchMode": 0,
            "PoolSolutions": 1,
            "FeasibilityTol": 1e-9,
            # "MIPFocus": 1,
            "OptimalityTol": 1e-2,
            "IntFeasTol": 1e-9,
        }

        model = GurobiInterface.construct_gurobi_model(system, settings=settings)
        model.setParam("TimeLimit", 10)
        # flux_variables = {"X_gluc_model", "X_ac_model"}
        GurobiInterface.relax_eqs(model)
        vars = model.getVars()
        # set relaxation to zero
        # rel_start = {var.VarName: 0 for var in vars if 'relax' in var.VarName}
        force_fluxes = True
        space = np.linspace(0, 2, 3)
        derivatives = SystemDerivatives(system)
        for num in space:
            partial_start = {"X_gluc_model": num}
            # partial_start.update(rel_start)
            GurobiInterface.set_starting_point(model, partial_start, force_fluxes)
            model.optimize()
            num_subs = {
                var.VarName: var.X for var in vars if var.VarName in system.sym_dict
            }

            x0 = np.array([val for key, val in sorted(num_subs.items())])
            x0 += np.random.normal(size=x0.size, scale=0.1)
            anal_jac = SystemEvaluator.jac_eval(
                x0, system=system, derivatives=derivatives
            )
            num_jac = f_jacobian_wrapped(x0, system)

            viol = SystemEvaluator.feval(x0, system=system)
            diff = np.abs(num_jac) - np.abs(anal_jac)
            self.assertTrue(np.max(np.abs(diff)) < 1e-4)


class Tester(unittest.TestCase):
    @staticmethod
    def create_min_env(org_names, D=0.5, C=(2, 2), uptake_prop=2):
        org_dict = {name: minimal_stoichiometric_model(name) for name in org_names}
        # for model in org_dict.values():
        #     purge_underscore(model)
        consortium = Consortium(org_dict, uptake_prop=uptake_prop)
        # make a chemical environment
        extracellular = {
            met.id
            for met in list(org_dict.values())[0].metabolites
            if met.compartment == "e"
        }
        environment = ChemicalEnvironment(
            D=D, C_supply=pd.Series(C, index=sorted(extracellular))
        )
        chemostatSystem = ChemostatSystemRA(consortium, environment)
        return chemostatSystem, environment, consortium

if __name__ == "__main__":
    unittest.main()
