# ©2022 ETH Zurich, Axel Theorell

import numpy as np
from scipy.optimize import least_squares
from sympy.core.add import Add
from sympy.core.symbol import Symbol
from sympy.core.mul import Mul
from symbolic_system.make_nl_system import SystemUtils
from symbolic_system.system_evaluator import SystemEvaluator
from sympy.core.numbers import One, NegativeOne, Float
from scipy.optimize._numdiff import approx_derivative
from symbolic_system.naming_utils import NamingUtils


def least_squares_solve(system, gurobi_vars, derivatives):
    num_subs = {
        var.VarName: var.X for var in gurobi_vars if var.VarName in system.sym_dict
    }
    x0 = np.array([val for key, val in sorted(num_subs.items())])
    # bounds = make_variable_bounds_vector(system)
    # # num = SystemEvaluator.feval(num_subs, system=system)
    # lb_diff = np.minimum(x0-bounds[0,:], 0)
    # ub_diff = np.maximum(x0-bounds[1,:], 0)
    # x0 = x0 - lb_diff + ub_diff
    res = least_squares(
        SystemEvaluator.feval,
        x0,
        verbose=2,
        ftol=1e-5,
        jac=SystemEvaluator.jac_eval,
        method="lm",
        kwargs={"system": system, "derivatives": derivatives},
    )
    return res


def f_jacobian_wrapped(x, system):
    J = approx_derivative(SystemEvaluator.feval, x, method="2-point", kwargs={"system": system})
    if J.ndim != 2:  # J is guaranteed not sparse.
        J = np.atleast_2d(J)

    return J


def finite_difference_jacobian(system, gurobi_vars):
    num_subs = {
        var.VarName: var.X for var in gurobi_vars if var.VarName in system.sym_dict
    }
    x0 = np.array([val for key, val in sorted(num_subs.items())])

    J0 = f_jacobian_wrapped(x0, system)
    return J0


def make_variable_bounds_vector(system):
    bounds_expr_dict = {
        **system.ineq_dict[NamingUtils.FLUX_BOUNDS_NAME],
        **system.ineq_dict[NamingUtils.CONCENTRATION_BOUNDS_NAME],
        **system.ineq_dict[NamingUtils.LAGRANGE_BOUNDS_NAME],
    }
    bounds = list()
    for key, val in sorted(system.sym_dict.items()):
        # TODO: improve naming scheme of lagrange multipliers to catch all lower bounds
        # start with lower bound
        lb = -np.inf
        if NamingUtils.var_lb_name(key) in bounds_expr_dict:
            bound_expr = bounds_expr_dict[NamingUtils.var_lb_name(key)]
            if isinstance(bound_expr, Add):
                # this is the only case we handle
                if len(bound_expr.free_symbols) == 1 and len(bound_expr.args) == 2:
                    i = 0
                    while i < len(bound_expr.args):
                        arg = bound_expr.args[i]
                        if not isinstance(arg, Float):
                            assert isinstance(arg, Mul)
                        else:
                            lb = float(arg)
                        i += 1
                    assert lb > -np.inf
                # elif len(bound_expr.free_symbols) == 2:
                #     raise ValueError

            elif isinstance(bound_expr, Mul):
                # again, this is the only case we handle
                if len(bound_expr.free_symbols) == 1 and len(bound_expr.args) == 2:
                    i = 0
                    while i < len(bound_expr.args):
                        arg = bound_expr.args[i]
                        if not isinstance(arg, NegativeOne):
                            assert isinstance(arg, Symbol)
                        else:
                            lb = 0
                        i += 1
                    assert lb > -np.inf
                # elif len(bound_expr.free_symbols) == 2:
                #     raise ValueError
        # if np.isinf(lb):
        #     if not key.startswith(NamingUtils.LAGRANGE_EQ_NAME):
        #         raise Exception
        ub = np.inf
        if NamingUtils.var_ub_name(key) in bounds_expr_dict:
            bound_expr = bounds_expr_dict[NamingUtils.var_ub_name(key)]
            if isinstance(bound_expr, Add):
                # this is the only case we handle
                if len(bound_expr.free_symbols) == 1 and len(bound_expr.args) == 2:
                    i = 0
                    while i < len(bound_expr.args):
                        arg = bound_expr.args[i]
                        if not isinstance(arg, Float):
                            assert isinstance(arg, Symbol)
                        else:
                            ub = -float(arg)
                        i += 1
                    assert ub < np.inf
                # elif len(bound_expr.free_symbols) == 2:
                #     raise ValueError

            elif isinstance(bound_expr, Symbol):
                # again, this is the only case we handle
                ub = 0
        bounds.append([lb, ub])
    bounds = np.array(bounds).T
    diff = bounds[1, :] - bounds[0, :]
    assert np.all(diff) > 0
    return bounds
