# ©2022 ETH Zurich, Axel Theorell

from pyscipopt import Model
from sympy.core.numbers import One, NegativeOne, Float
from sympy.core.add import Add
from sympy.core.symbol import Symbol
from sympy.core.mul import Mul
from symbolic_system.specific_systems import SymbolicSystem
import numpy as np
import pandas as pd
from typing import Dict
from numeric_solution.gurobi_interface import GurobiInterface


def solve_system_with_scip(
    system: SymbolicSystem,
    partial_start: Dict[str, int] = None,
    settings: Dict[str, int] = None,
    force_fluxes: bool = False,
):
    if partial_start is None:
        partial_start = {}
    model = Model()
    var_keys = list(system.sym_dict.keys())
    # add boolean variables for the c_s_eqs and growth_eqs
    bin_var_keys = [name + "Binary" for name in system.eq_dict["c_s_eqs"]] + [
        name + "Binary" for name in system.eq_dict["growth_eqs"]
    ]
    vars = {key: model.addVar(key, lb=None) for key in var_keys}
    vars.update({key: model.addVar(key, vtype="B", lb=0, ub=1) for key in bin_var_keys})
    constraints = list()
    types = ["eq_dict", "ineq_dict"]
    for type in types:
        for constr_class_key, constr_class in system.__getattribute__(type).items():
            for name, expr in constr_class.items():
                if type == "eq_dict":
                    if constr_class_key == "c_s_eqs":
                        gur_expr = expr_maker(expr, vars, 1, special_eq=name)
                        # print(gur_expr)
                    elif constr_class_key == "growth_eqs":
                        gur_expr = expr_maker(expr, vars, 1, special_eq=name)
                        # constraints.append(optlang.Constraint(expr, lb=0, ub=0, name=name + "EQ"))
                    else:
                        gur_expr = expr_maker(expr, vars, 1)
                    constraints.append(model.addCons(gur_expr == 0, name=name + "EQ"))
                else:
                    assert type == "ineq_dict"
                    gur_expr = expr_maker(expr, vars, 1)
                    constraints.append(model.addCons(gur_expr <= 0, name=name))
    # set_starting_point(vars, partial_start, force_fluxes)
    # model.update()
    # create active constraint based objective
    # binary_vars = np.array([vars[var_name] for var_name in sorted(vars) if vars[var_name].VType == "B"])
    # # give random sorted coeffients
    # coeffients = np.array(sorted(np.random.rand(len(binary_vars)), reverse=True))
    # obj_expression = np.sum(binary_vars * coeffients)
    obj_expression = np.sum([var for var in vars.values() if var.name.startswith("X")])
    model.setObjective(-obj_expression)
    # model.writeLP()
    model.optimize()
    sol = model.getBestSol()
    sol = pd.Series({key.name: sol[key] for key in vars.values()})
    status = model.getStatus()

    assert status == "optimal"

    return sol
