# ©2022 ETH Zurich, Axel Theorell

class GurobiNamer:

    BINARY_NAME = "Binary"

    @staticmethod
    def binary_variable_name(name):
        if name.endswith(GurobiNamer.BINARY_NAME):
            raise NameError("Variable name is already on binary form")
        return name + GurobiNamer.BINARY_NAME

    @staticmethod
    def pos_relax_variable(constr_name):
        return "pos_relax_" + constr_name

    @staticmethod
    def neg_relax_variable(constr_name):
        return "neg_relax_" + constr_name

    @staticmethod
    def is_binary_name_type(name):
        try:
            GurobiNamer.binary_variable_name(name)
            return False
        except NameError:
            return True
