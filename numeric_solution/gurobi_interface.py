# ©2022 ETH Zurich, Axel Theorell

from sympy.core.numbers import One, NegativeOne, Float
from sympy.core.add import Add
from sympy.core.symbol import Symbol
from sympy.core.mul import Mul
import gurobipy as gp
from symbolic_system.specific_systems import SymbolicSystem
import numpy as np
import pandas as pd
from typing import Dict
from numeric_solution.gurobi_namer import GurobiNamer
from symbolic_system.make_nl_system import SystemUtils
from symbolic_system.naming_utils import NamingUtils
from numbers import Number


class GurobiInterface:
    @staticmethod
    def relax_eqs(model):
        # get all extracellular constraints (they are QConstrs)
        ub = np.inf
        q_constrs = [
            constr
            for constr in model.getQConstrs()
            if (constr.QCName.endswith(NamingUtils.EXTRACELLULAR_EQ_NAME)) or (NamingUtils.LAGRANGE_EQ_NAME in constr.QCName)
        ]
        new_obj_expr = 0
        for q_constr in q_constrs:
            expr = model.getQCRow(q_constr)
            relevant = False
            for i in range(expr.size()):
                temp_var1 = expr.getVar1(i)
                temp_var2 = expr.getVar2(i)
                if (temp_var1.VarName.startswith(NamingUtils.BIOMASS_VARIABLE_PREFIX)) or (temp_var2.VarName.startswith(NamingUtils.BIOMASS_VARIABLE_PREFIX)):
                    relevant = True
                    break
            # also check linear part
            lin_expr = expr.getLinExpr()
            for i in range(lin_expr.size()):
                temp_var = lin_expr.getVar(i)
                if temp_var.VarName.startswith(NamingUtils.BIOMASS_VARIABLE_PREFIX):
                    relevant = True
                    break
            if relevant:
                relax_vars = [
                    model.addVar(
                        name=GurobiNamer.pos_relax_variable(q_constr.QCName), ub=ub
                    ),
                    model.addVar(
                        name=GurobiNamer.neg_relax_variable(q_constr.QCName), ub=ub
                    ),
                ]
                expr = expr + relax_vars[0] - relax_vars[1]
                model.remove(q_constr)
                model.addConstr(
                    expr, sense=q_constr.QCSense, rhs=q_constr.QCRHS, name=q_constr.QCName
                )
                new_obj_expr += gp.quicksum(relax_vars)
                # new_obj_expr += relax_vars[0]*relax_vars[0] + relax_vars[1]*relax_vars[1]
        # Now do corresponding necessary relaxation in linear constraints
        l_constrs = [
            constr
            for constr in model.getConstrs()
            if (NamingUtils.GROWTH_EQ_NAME in constr.ConstrName) or (NamingUtils.LAGRANGE_EQ_NAME in constr.ConstrName)
        ]
        for l_constr in l_constrs:
            expr = model.getRow(l_constr)
            # check if biomass is in the linear expression
            relevant = False
            for i in range(expr.size()):
                temp_var = expr.getVar(i)
                if temp_var.VarName.startswith(NamingUtils.BIOMASS_VARIABLE_PREFIX):
                    relevant = True
                    break
            if relevant:
                relax_vars = [
                    model.addVar(
                        name=GurobiNamer.pos_relax_variable(l_constr.ConstrName), ub=ub
                    ),
                    model.addVar(
                        name=GurobiNamer.neg_relax_variable(l_constr.ConstrName), ub=ub
                    ),
                ]
                expr = expr + relax_vars[0] - relax_vars[1]
                model.remove(l_constr)
                model.addConstr(
                    expr, sense=l_constr.Sense, rhs=l_constr.RHS, name=l_constr.ConstrName
                )
                new_obj_expr += gp.quicksum(relax_vars)
                # new_obj_expr += relax_vars[0]*relax_vars[0] + relax_vars[1]*relax_vars[1]

        model.setObjective(new_obj_expr, gp.GRB.MINIMIZE)
        model.update()
        # vars = [var for var in model.getVars() if "relax" in var.VarName]
        # pass

    @staticmethod
    def expr_maker(sym, vars, coeff, special_eq=""):
        if isinstance(sym, Add):
            assert coeff == 1
            add = np.sum(
                [
                    GurobiInterface.expr_maker(sym_, vars, coeff_)
                    for sym_, coeff_ in sym.as_coefficients_dict().items()
                ]
            )
            return add
        elif isinstance(sym, Symbol):
            return vars[sym.name] * coeff
        elif isinstance(sym, Mul):
            if special_eq != "":
                args = [arg for arg in sym.args if not isinstance(arg, NegativeOne)]
                if len(args) == len(sym.args) - 1:
                    coeff *= -1
                assert len(args) == 2
                prod = coeff * np.array(
                    [
                        GurobiInterface.expr_maker(args[0], vars, 1),
                        GurobiInterface.expr_maker(args[1], vars, 1),
                    ]
                )
                return prod
            else:
                prod = (
                    np.product(
                        [GurobiInterface.expr_maker(arg, vars, 1) for arg in sym.args]
                    )
                    * coeff
                )
                return prod
        elif isinstance(sym, One):
            return coeff
        elif isinstance(sym, NegativeOne):
            return -coeff
        elif isinstance(sym, Float):
            return float(coeff * sym)
        else:
            raise TypeError("unexpected type " + str(type(sym)) + " encountered")

    @staticmethod
    def set_starting_point(model, partial_start, force_fluxes):
        for key, val in partial_start.items():
            var = model.getVarByName(key)
            if force_fluxes:
                var.UB = val
                var.LB = val
                var.Start = gp.GRB.UNDEFINED
            else:
                var.Start = val

    @staticmethod
    def construct_gurobi_model(
        system: SymbolicSystem,
        settings: Dict[str, Number] = None,
        binarize=True,
        omega=1e4,
    ):
        model = gp.Model()
        model.setParam("IntFeasTol", 1e-7)
        model.setParam("PoolSolutions", 10)
        if settings is not None:
            for key, val in settings.items():
                model.setParam(key, val)
        # model.params.SolutionLimit = 10
        var_keys = list(system.sym_dict.keys())
        # add boolean variables for the c_s_eqs and growth_eqs
        if binarize:
            var_keys += [
                GurobiNamer.binary_variable_name(name)
                for name in system.eq_dict[NamingUtils.COMPLEMENTARY_SLACKNESS_NAME]
            ] + [
                GurobiNamer.binary_variable_name(name)
                for name in system.eq_dict[NamingUtils.GROWTH_EQ_NAME]
            ]
        vars = model.addVars(var_keys, lb=-np.inf, ub=np.inf, name=var_keys)
        for var_name, var in vars.items():
            if GurobiNamer.is_binary_name_type(var_name):
                var.VType = gp.GRB.BINARY
                var.LB = 0
                var.UB = 1
        constraints = list()
        types = ["eq_dict", "ineq_dict"]
        for type in types:
            for constr_class_key, constr_class in system.__getattribute__(type).items():
                for name, expr in constr_class.items():
                    if type == "eq_dict":
                        if (
                            constr_class_key == NamingUtils.GROWTH_EQ_NAME
                            or constr_class_key
                            == NamingUtils.COMPLEMENTARY_SLACKNESS_NAME
                        ) and binarize:
                            gur_expr = GurobiInterface.expr_maker(
                                expr, vars, 1, special_eq=name
                            )

                            constraints.append(
                                model.addConstr(
                                    gur_expr[0]
                                    - vars[GurobiNamer.binary_variable_name(name)]
                                    * omega,
                                    sense="<=",
                                    rhs=0,
                                    name="_".join([name, constr_class_key, "1"]),
                                )
                            )
                            constraints.append(
                                model.addConstr(
                                    gur_expr[0]
                                    + vars[GurobiNamer.binary_variable_name(name)]
                                    * omega,
                                    sense=">=",
                                    rhs=0,
                                    name="_".join([name, constr_class_key, "2"]),
                                )
                            )
                            constraints.append(
                                model.addConstr(
                                    gur_expr[1]
                                    - (1 - vars[GurobiNamer.binary_variable_name(name)])
                                    * omega,
                                    sense="<=",
                                    rhs=0,
                                    name="_".join([name, constr_class_key, "3"]),
                                )
                            )
                            constraints.append(
                                model.addConstr(
                                    gur_expr[1]
                                    + (1 - vars[GurobiNamer.binary_variable_name(name)])
                                    * omega,
                                    sense=">=",
                                    rhs=0,
                                    name="_".join([name, constr_class_key, "4"]),
                                )
                            )

                        #     gur_expr = expr_maker(expr, vars, 1, special_eq=name)
                        #     # print(gur_expr)
                        # elif constr_class_key == "growth_eqs":
                        #     gur_expr = expr_maker(expr, vars, 1, special_eq=name)
                        #     # constraints.append(optlang.Constraint(expr, lb=0, ub=0, name=name + "EQ"))
                        else:
                            gur_expr = GurobiInterface.expr_maker(expr, vars, 1)
                            constraints.append(
                                model.addConstr(
                                    gur_expr,
                                    sense="=",
                                    rhs=0,
                                    name="_".join([name, constr_class_key]),
                                )
                            )
                    else:
                        assert type == "ineq_dict"
                        gur_expr = GurobiInterface.expr_maker(expr, vars, 1)
                        constraints.append(
                            model.addConstr(
                                gur_expr,
                                sense="<=",
                                rhs=0,
                                name="_".join([name, constr_class_key]),
                            )
                        )

        model.update()

        obj_expression = np.sum(
            [var for var in vars.values() if var.VarName.startswith("X")]
        )

        model.setObjective(obj_expression, gp.GRB.MAXIMIZE)
        return model

    @staticmethod
    def solve_system_with_gurobi(
        system: SymbolicSystem,
        partial_start: Dict[str, Number] = None,
        settings: Dict[str, int] = None,
        force_fluxes: bool = False,
        binarize=True,
        omega=1e4,
    ):
        model = GurobiInterface.construct_gurobi_model(
            system, settings=settings, binarize=binarize, omega=omega
        )
        if force_fluxes:
            model.setParam("nonConvex", 1)
        else:
            model.setParam("nonConvex", 2)
        if partial_start is None:
            partial_start = {}
        vars = model.getVars()
        vars = {var.VarName: var for var in vars}
        GurobiInterface.set_starting_point(model, partial_start, force_fluxes)
        model.optimize()

        assert model.Status == gp.GRB.OPTIMAL
        vars = pd.Series(vars)
        solutions = list()
        for i in range(model.SolCount):
            model.params.SolutionNumber = i
            solutions.append(pd.Series(model.Xn, vars.index))
        return solutions
