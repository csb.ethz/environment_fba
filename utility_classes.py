# ©2022 ETH Zurich, Axel Theorell

import pandas as pd
import cobra
import numpy as np


class ChemicalEnvironment:
    def __init__(self, D: float = 1, C_supply: pd.Series = pd.Series()):
        self.C_supply = C_supply
        self.D = D
        self.vars = list(self.C_supply.index)


class Consortium:
    def __init__(self, organism_dict: dict, uptake_prop: float = 1):
        self.organism_dict = organism_dict
        self.X_vars = ["X_" + org for org in organism_dict.keys()]
        self.flux_vars = dict()
        self.uptake_prop = uptake_prop
        for key, model in organism_dict.items():
            self.flux_vars[key] = [r.id for r in model.reactions]


class ModellingUtilities:
    @staticmethod
    def make_irreversible_cobra(model, subset):
        """Makes reactions in subset of COBRA model positive and irreversible."""

        if not subset:
            subset = set(model.reactions)
        else:
            subset = set(subset)
            for x in subset:
                if isinstance(x, str):
                    subset.remove(x)
                    subset.add(model.reactions.get_by_id(x))

        new_reactions = []

        for r in list(subset):
            if r.lower_bound < 0:
                if r.upper_bound > 0:
                    # Create new reverse reaction
                    r_rev = cobra.Reaction(r.id + "_rev")
                    r_rev.add_metabolites({m: -c for m, c in r.metabolites.items()})
                    r_rev.bounds = np.max([0, -r.upper_bound]), -r.lower_bound
                    new_reactions.append(r_rev)
                    subset.add(r_rev)
                else:
                    # Reverse existing reaction
                    r.bounds = np.max([0, -r.upper_bound]), -r.lower_bound
                    r.add_metabolites({m: -2 * c for m, c in r.metabolites.items()})
                    # Remove reaction to avoid reported bug with Gurobi 9
                    model.remove_reactions([r])
                    r.id += "_rev"
                    new_reactions.append(r)
            if r.upper_bound > 0:
                # Make existing reaction irreversible
                r.lower_bound = np.max([0, r.lower_bound])

        model.repair()
        model.add_reactions(new_reactions)

        return subset
