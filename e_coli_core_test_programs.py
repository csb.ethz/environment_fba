# ©2022 ETH Zurich, Axel Theorell
from numeric_solution.gurobi_interface import GurobiInterface
import cobra
from symbolic_system.naming_utils import NamingUtils
from numeric_solution.scipy_root_solver import (
    least_squares_solve,
    make_variable_bounds_vector,
)
from symbolic_system.specific_systems import ChemostatSystemRA, ChemostatSystemRC
from utility_classes import ChemicalEnvironment, Consortium
import numpy as np
import pandas as pd
from symbolic_system.system_evaluator import SystemEvaluator
from sympy.solvers.solveset import linsolve
import mptool
from symbolic_system.derivatives import SystemDerivatives
from e_coli_core_ensemble_construction import make_ac_gluc_system, make_mutually_dependent_community



def test_e_coli_zero_solution(system, C_supply, fluxes):
    syms = system.sym_dict.copy()
    extr_concentration = {syms[key]: val for key, val in C_supply.iteritems()}
    [syms.pop(key.name) for key in extr_concentration]
    fluxes_and_b_mass = {
        syms[key]: fluxes[key.split("_gluc_model")[0]]
        for key in syms
        if key.split("_gluc_model")[0] in fluxes.index
    }
    fluxes_and_b_mass[syms["X_gluc_model"]] = 0
    fluxes_and_b_mass[syms["Mu_gluc_model"]] = fluxes["Mu_gluc_model"]
    [syms.pop(key.name) for key in fluxes_and_b_mass]

    result = extr_concentration.copy()
    result.update(fluxes_and_b_mass)

    # check inequalities
    # ineq_substituted = SystemEvaluator.eval_ineqs(system, result)
    # simplified_lagrange = {key: val.subs(ub_lagrange) for key, val in system.eq_dict['lagrange_eqs'].items()}
    substituted = SystemEvaluator.eval_eqs(system, result)
    # show the inconsistency
    eqs = [eq for eq in substituted["c_s_eqs"].values()] + [
        eq for eq in substituted["lagrange_eqs"].values()
    ]
    la_syms = [sym for sym in syms.values()]
    res = linsolve(eqs, la_syms)
    # assert len(res) > 0
    # take all lagrange multipliers from c_s_eqs that you can set to zero
    # zero_lagrange = {}
    # for expr in substituted['c_s_eqs'].values():
    #     if len(expr.free_symbols) == 1:
    #         zero_lagrange[list(expr.free_symbols)[0]] = 0
    #     elif len(expr.free_symbols) > 1:
    #         raise ValueError('Non-obvious value of lagrange multiplier')
    # [syms.pop(key.name) for key in zero_lagrange]
    # result.update(zero_lagrange)
    # substituted_lagrange = dict()
    # for eq_name, expr in substituted['lagrange_eqs'].items():
    #     substituted_lagrange[eq_name] = expr.subs(result)
    # res = linsolve([eq for eq in substituted_lagrange.values()], [sym for sym in syms.values()])
    # sym_ordered = [sym for name, sym in sorted(syms.items())]
    # eq_ordered = pd.Series({eq_name: eq for eq_name, eq in sorted(substituted_lagrange.items())})
    # matrix = sympy.linear_eq_to_matrix(eq_ordered.values, sym_ordered)
    # np_repr = np.array(matrix[0])
    # frame = pd.DataFrame(np_repr, index=eq_ordered.index, columns=sym_ordered)
    # frame = frame.apply(pd.to_numeric)
    # null_space = scipy.linalg.null_space(frame.values)
    result = {sym.name: val for sym, val in result.items()}
    return result


def pair_coli_root_CC():
    D=0.1
    system = make_ac_gluc_system(system_type="CC", D=D)
    settings = {
        "PoolSearchMode": 0,
        "PoolSolutions": 1,
        "FeasibilityTol": 1e-7,
        "TimeLimit": 200,
        "OptimalityTol": 1e-5,
        "IntFeasTol": 1e-8,
    }

    model = GurobiInterface.construct_gurobi_model(system, settings=settings)
    # flux_variables = {"X_gluc_model", "X_ac_model"}
    GurobiInterface.relax_eqs(model)
    vars = model.getVars()
    # set relaxation to zero
    # rel_start = {var.VarName: 0 for var in vars if 'relax' in var.VarName}
    force_fluxes = True
    space = np.linspace(0, 1, 3)
    grid = np.array([[0.5555555555556*2, 0], [1.226, 0.212], [1.233, 0], [0, 0.249], [0,0]])*0.5
    # grid = [[1.2, 0], [0, 0.2], [1.2, 0.2]]
    derivatives = SystemDerivatives(system)
    solutions = list()
    for i in range(len(grid)):
        partial_start = {
            "X_gluc_model": grid[i][0],
            "X_ac_model": grid[i][1],
        }
        # if grid[i][0] != 0:
        #     partial_start["Mu_gluc_model"] = D
        # if grid[i][1] != 0:
        #     partial_start["Mu_ac_model"] = D
        # partial_start.update(rel_start)
        GurobiInterface.set_starting_point(model, partial_start, force_fluxes)
        model.optimize()
        subs_MILP = {
            system.sym_dict[var.VarName]: var.X
            for var in vars
            if var.VarName in system.sym_dict
        }
        substituted_MILP = SystemEvaluator.eval(system, subs_MILP)
        violation_MILP = SystemEvaluator.max_violation(substituted_MILP)

        res = least_squares_solve(system, vars, derivatives)
        x_least_squares = pd.Series(res.x, index=sorted(system.sym_dict.keys()))
        subs = {system.sym_dict[key]: val for key, val in x_least_squares.iteritems()}
        substituted_least_squares = SystemEvaluator.eval(system, subs)
        violation_least_squares = SystemEvaluator.max_violation(
            substituted_least_squares
        )
        solutions.append(
            {
                "x_least_squares": x_least_squares,
                "violation_MILP": violation_MILP,
                "violation_ls": violation_least_squares,
                "substituted_MILP": substituted_MILP,
                "substituted_least_squares": substituted_least_squares,
            }
        )
        print("done")

def mutually_dependent_coli():
    D=0.1
    system = make_mutually_dependent_community(system_type="CA", D=D)
    settings = {
        "PoolSearchMode": 0,
        "PoolSolutions": 1,
        "FeasibilityTol": 1e-7,
        "TimeLimit": 200,
        "OptimalityTol": 1e-5,
        "IntFeasTol": 1e-8,
    }

    model = GurobiInterface.construct_gurobi_model(system, settings=settings)
    # flux_variables = {"X_gluc_model", "X_ac_model"}
    GurobiInterface.relax_eqs(model)
    vars = model.getVars()
    # set relaxation to zero
    # rel_start = {var.VarName: 0 for var in vars if 'relax' in var.VarName}
    force_fluxes = True
    space = np.linspace(0, 1, 3)
    grid = np.array([[0.5555555555556*2, 0], [1.226, 0.212], [1.233, 0], [0, 0.249], [0,0]])*0.5
    # grid = [[1.2, 0], [0, 0.2], [1.2, 0.2]]
    derivatives = SystemDerivatives(system)
    solutions = list()
    var_names = sorted([key for key in system.sym_dict if key.startswith(NamingUtils.BIOMASS_VARIABLE_PREFIX)])
    for i in range(len(grid)):
        partial_start = {
            var_names[0]: grid[i][0],
            var_names[1]: grid[i][1],
        }
        # if grid[i][0] != 0:
        #     partial_start["Mu_gluc_model"] = D
        # if grid[i][1] != 0:
        #     partial_start["Mu_ac_model"] = D
        # partial_start.update(rel_start)
        GurobiInterface.set_starting_point(model, partial_start, force_fluxes)
        model.optimize()
        subs_MILP = {
            system.sym_dict[var.VarName]: var.X
            for var in vars
            if var.VarName in system.sym_dict
        }
        substituted_MILP = SystemEvaluator.eval(system, subs_MILP)
        violation_MILP = SystemEvaluator.max_violation(substituted_MILP)

        res = least_squares_solve(system, vars, derivatives)
        x_least_squares = pd.Series(res.x, index=sorted(system.sym_dict.keys()))
        subs = {system.sym_dict[key]: val for key, val in x_least_squares.iteritems()}
        substituted_least_squares = SystemEvaluator.eval(system, subs)
        violation_least_squares = SystemEvaluator.max_violation(
            substituted_least_squares
        )
        solutions.append(
            {
                "x_least_squares": x_least_squares,
                "violation_MILP": violation_MILP,
                "violation_ls": violation_least_squares,
                "substituted_MILP": substituted_MILP,
                "substituted_least_squares": substituted_least_squares,
            }
        )
        print("done")


if __name__ == "__main__":
    # single_models()
    # pair_model()
    # pair_coli_root_CC()
    #feasibility_iterative()
    mutually_dependent_coli()


