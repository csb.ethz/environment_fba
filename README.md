To set up a python environment that works with this package, do the following preparatory steps.

Get anaconda, for example from https://www.anaconda.com/products/individual
Note that, anaconda is used because it has the easiest gurobi integration. Gurobi is a strict dependency of 
decision_modelling. To use gurobi, a gurobi  installation with a valid license is necessary 
(free for academic use). Gurobi licenses are obtained at https://www.gurobi.com/

First create a new python environment (here named decision_modelling) and activate it

    conda create --name decision_modelling python=3.7
    
    conda activate decision_modelling

Install additional dependencies 

    pip install -r requirements.txt
    
Install gurobi through anaconda

    conda install -c gurobi gurobi


Results for the gln/ala syntrophic community are generated with the command:

    python run_from_shell_script.py
   
This will run the program with the default arguments 2500 grid points in the space [0, 1] x [0, 1] for CA.
To run using some other configuration use:

    python run_from_shell_script.py grid_points grid_max system_type
    
where grid_points and grid_max are integers and system_type is either CA or CC. grid_points, being the number 
of points in a regular quadratic grid, must be the square of some number 1, 4, 9, 16, 25 etc.

For a faster serial test execution, try the call:

    python run_from_shell_script.py 4 1 CA

The above examples yield serial execution of the program, which, for large numbers of grid points (as the default 2500 
used in the paper), takes a long time. To speed things up, the program supports parallel execution. To facilitate an 
outer parallelize loop, the program supports an extra input, current_point, which specifies a specific point in the grid 
which should be executed (and no other point). Inside some (parallel) loop, the call is thus:

    python run_from_shell_script.py grid_points grid_max current_point system_type
 
Any execution of run_from_shell_script.py generates a new folder in the folder output, starting with the current date,
and ending with the commit hash and the system_type. Inside the folder, #grid_points pickle files are creating,
containing the information of each grid point are stored. To extract and combine the information in these files, the 
script process_shell_output.py is used:

    python process_shell_output.py name_of_directory_inside_output_directory

This will create graphical, csv and json outputs, summarizing the results, all disposed in the same folder as where the 
pickle files were found. 

Recreating the results of the prisoners dilemma and coexistence examples requires a Mathematica license, which is not 
free, even for academic use (https://www.wolfram.com/mathematica/). Having Mathematica installed, to reproduce the 
examples, check out their respective git branches, 'chemostat' for coexistence and 'CaiChan' for prisoners dilemma and 
run the respective scripts:
    
    python chemostat_minimal.py
    
    python Cai_chan_models.py

 