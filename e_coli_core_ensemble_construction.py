# ©2022 ETH Zurich, Axel Theorell

import cobra
from symbolic_system.specific_systems import ChemostatSystemRA, ChemostatSystemRC
from utility_classes import ChemicalEnvironment, Consortium
import numpy as np
import pandas as pd
import mptool
import os
from cobrapy_bigg_client import client

def get_transport_reactions(model):
    return [
        r
        for r in list(model.reactions)
        if "e" in r.compartments and r not in model.exchanges
    ]


def prefix_numeral_reactions_metabolites(model, prefix="R_"):
    """
    prefixes all reactions in a model to avoid forbidden reaction names in cobra
    :param model:
    :param prefix:
    :return:
    """
    reactions = list(r for r in model.reactions if r.id[0].isdigit())
    model.remove_reactions(reactions)
    for r in reactions:
        if not r.id.startswith(prefix):
            # model.remove_reactions([r])
            r.id = prefix + r.id
            # model.add_reactions([r])
    model.add_reactions(reactions)
    model.repair()

    mets = list(m for m in model.metabolites if m.id[0].isdigit())
    for m in mets:
        m.id = "M_" + m.id
    model.repair()
    return model


def limit_transporters(model, val):
    for r in get_transport_reactions(model):
        reactant_compartments = [m.compartment for m in r.reactants]
        if "e" in reactant_compartments:
            r.upper_bound = val
        pass


def system_from_models(
    org_dict, exc_conc, sol_dict=None, compounds_in_medium=None, D=1.0, system_type="CA"
):

    for name, model in org_dict.items():
        # remove all exchange fluxes
        model.remove_reactions([r for r in list(model.exchanges)])
        # remove the ATPM reaction
        # model.remove_reactions([model.reactions.ATPM])
        # rename growth reactions and make reactions model specific
        bm = model.reactions.BIOMASS_Ecoli_core_w_GAM
        reactions = list(model.reactions)
        model.remove_reactions(reactions)
        for r in reactions:
            r.id = r.id + "_" + name

            # if r.lower_bound < 0:
            #     r.lower_bound = -np.inf
        bm.id = "Mu_" + name
        model.add_reactions(reactions)
        model.repair()
        # put the growth rate into the flux solution with the new name
        if not sol_dict is None:
            sol_dict[name].fluxes["Mu_" + name] = sol_dict[
                name
            ].fluxes.BIOMASS_Ecoli_core_w_GAM

    uptake_prop = 1
    consortium = Consortium(org_dict, uptake_prop=uptake_prop)
    # Create extracellular pool variables
    active_extracellular = {
        m.id for m in list(org_dict.values())[0].metabolites if m.compartment == "e"
    }
    if compounds_in_medium is None:
        C_supply = pd.Series({r: exc_conc for r in active_extracellular})
    else:
        for key in compounds_in_medium:
            assert key in active_extracellular
        for key in active_extracellular:
            if key not in compounds_in_medium:
                compounds_in_medium[key] = 0
        C_supply = pd.Series(compounds_in_medium)
    environment = ChemicalEnvironment(D=D, C_supply=C_supply)
    if system_type == "CA":
        system = ChemostatSystemRA(consortium, environment)
    elif system_type == "CC":
        system = ChemostatSystemRC(consortium, environment)
    else:
        raise ValueError
    return system, C_supply


def initialize_single_model(exc_conc=None):
    gluc_model = client.download_model("e_coli_core", save=False)
    gluc_model = prefix_numeral_reactions_metabolites(gluc_model)
    # make transporters irreversible
    transporters = get_transport_reactions(gluc_model)
    test = mptool._make_irreversible_cobra(gluc_model, transporters)
    gluc_model.solver = "glpk_exact"
    superfluous_uptake_transporters = ["AKGt2r", "ACALDt", "PYRt2", "CO2t", "SUCCt2_2", "D_LACt2", "SUCCt3", "ETOHt2r",
                                       "FORt2", "FRUpts2", "FUMt2_2", "MALt2_2"]
    # remove the superfluous_uptake_transporters
    for reaction_string in superfluous_uptake_transporters:

        gluc_model.remove_reactions([gluc_model.reactions.get_by_id(reaction_string)])
    # import everything
    for r in list(gluc_model.exchanges):
        r.lower_bound = -100000
        r.upper_bound = 100000
    # gluc_model.reactions.EX_glc__D_e.lower_bound = -1000
    # if not no_removal:
    if exc_conc is not None:
        limit_transporters(gluc_model, exc_conc)
    return gluc_model





def full_initialization(
    removal_list, exc_conc, no_removal=False, compounds_in_medium=None, system_type="CA"
):
    gluc_model = initialize_single_model(exc_conc)
    # remove all intracellular metabolites except glucose
    if not no_removal:
        retained_intracellular = ["g6p_c", "pyr_c", "h_c"]
        met = [
            met
            for met in gluc_model.metabolites
            if (not met.compartment == "e")
            and (not any([met.id == retained for retained in retained_intracellular]))
        ]
        gluc_model.remove_metabolites(met)
        gluc_model.remove_reactions(
            [
                r
                for r in list(gluc_model.reactions)
                if len(r.metabolites) == 0
                or (
                    len(r.compartments) == 1
                    and "c" in r.compartments
                    and not r.id == "BIOMASS_Ecoli_core_w_GAM"
                )
            ]
        )
        # remove further reactions
        gluc_model.remove_reactions(
            [gluc_model.reactions.get_by_id(r_id) for r_id in removal_list]
        )  # , "D_LACt2", "ETOHt2r"]])
    # bound_single_model(gluc_model)

    gluc_sol = gluc_model.optimize()

    org_dict = {"gluc_model": gluc_model}
    sol_dict = {"gluc_model": gluc_sol}
    system, C_supply = system_from_models(
        org_dict, exc_conc, sol_dict=sol_dict, compounds_in_medium=compounds_in_medium, system_type=system_type
    )
    return org_dict, sol_dict, system, C_supply





def add_ala_depedency(model):
    compartments = ["e", "c"]
    new_metabolites = {'ala__L_' + comp: cobra.Metabolite('ala__L_' + comp, compartment=comp) for comp in compartments}
    model.add_metabolites(new_metabolites.values())
    ALATA_L = cobra.Reaction(id="ALATA_L", lower_bound=-1000, upper_bound=1000)
    ALATA_L.add_metabolites({
        model.metabolites.get_by_id("ala__L_c"): -1,
        model.metabolites.get_by_id("akg_c"): -1,
        model.metabolites.get_by_id("glu__L_c"): 1,
        model.metabolites.get_by_id("pyr_c"): 1,
    })
    ALAabc = cobra.Reaction(id="ALAabc", lower_bound=0, upper_bound=1000)
    ALAabc.add_metabolites({
        model.metabolites.get_by_id("ala__L_c"): 1,
        model.metabolites.get_by_id("adp_c"): 1,
        model.metabolites.get_by_id("h_c"): 1,
        model.metabolites.get_by_id("pi_c"): 1,
        model.metabolites.get_by_id("atp_c"): -1,
        model.metabolites.get_by_id("h2o_c"): -1,
        model.metabolites.get_by_id("ala__L_e"): -1,
    })
    # add secretion reactions
    ALA_sec = cobra.Reaction(id="ALA_sec", lower_bound=0, upper_bound=1000)
    ALA_sec.add_metabolites({
        model.metabolites.get_by_id("ala__L_c"): -1,
        model.metabolites.get_by_id("ala__L_e"): 1,
    })
    GLN_sec = cobra.Reaction(id="GLN_sec", lower_bound=0, upper_bound=1000)
    GLN_sec.add_metabolites({
        model.metabolites.get_by_id("gln__L_c"): -1,
        model.metabolites.get_by_id("gln__L_e"): 1,
    })
    model.add_reactions([ALATA_L, ALAabc, ALA_sec, GLN_sec])
    # add ala dependency in biomass equation
    model.reactions.get_by_id("BIOMASS_Ecoli_core_w_GAM").add_metabolites({
        model.metabolites.get_by_id("ala__L_c"): -0.513689
    })
    # only for the realistic check
    model.reactions.get_by_id("EX_glc__D_e").lower_bound = -10
    model.repair()
    with model as model:
        model.reactions.get_by_id("GLNabc").bounds = (0, 0)
        gln_prod_sol = model.optimize()
        assert gln_prod_sol.status == "optimal"
        assert gln_prod_sol.fluxes["BIOMASS_Ecoli_core_w_GAM"] > 1e-1
    # now check without glutamine production
    with model as model:
        model.reactions.get_by_id("GLNS").bounds = (0, 0)
        # we should still have flux
        gln_uptake_sol = model.optimize()
        assert gln_uptake_sol.status == "optimal"
        assert gln_uptake_sol.fluxes["BIOMASS_Ecoli_core_w_GAM"] > 1e-1
    with model as model:
        model.reactions.get_by_id("ALAabc").bounds = (0, 0)
        # we should still have flux
        ala_prod_sol = model.optimize()
        assert ala_prod_sol.status == "optimal"
        assert ala_prod_sol.fluxes["BIOMASS_Ecoli_core_w_GAM"] > 1e-1
    with model as model:
        model.reactions.get_by_id("ALATA_L").bounds = (0, 0)
        EX_ala__L_e = cobra.Reaction(id="EX_ala__L_e", lower_bound=-1000, upper_bound=1000)
        EX_ala__L_e.add_metabolites({model.metabolites.get_by_id("ala__L_e"): -1})
        model.add_reactions([EX_ala__L_e])
        # we should still have flux
        ala_uptake_sol = model.optimize()
        assert ala_uptake_sol.status == "optimal"
        assert ala_uptake_sol.fluxes["BIOMASS_Ecoli_core_w_GAM"] > 1e-1
    pass

def make_mutually_dependent_community(system_type="CC", D=0.1, only_ala=False, save_models=False):
    exc_conc = None

    ala_dep_model = initialize_single_model()
    add_ala_depedency(ala_dep_model)
    # ala_dep_model.reactions.get_by_id("ALATA_L").lower_bound = -10
    ala_dep_model.remove_reactions([ala_dep_model.reactions.get_by_id("ALATA_L")])
    # ala_dep_model.remove_reactions([ala_dep_model.reactions.get_by_id("ALAabc")])
    # now max growth should be zero
    ala_sol = ala_dep_model.optimize()
    # assert ala_sol.fluxes["BIOMASS_Ecoli_core_w_GAM"] < 1e-8

    gln_dep_model = initialize_single_model()
    add_ala_depedency(gln_dep_model)
    #the second deletion is only for the assert three lines later
    gln_dep_model.remove_reactions([gln_dep_model.reactions.get_by_id("GLNS"), gln_dep_model.reactions.get_by_id("EX_gln__L_e")])
    # now max growth should be zero
    gln_sol = gln_dep_model.optimize()
    if save_models:
        cobra.io.save_json_model(gln_dep_model, os.path.join("models","e_coli_core_gln_dep.json"))
        cobra.io.save_json_model(ala_dep_model, os.path.join("models", "e_coli_core_ala_dep.json"))

    assert gln_sol.fluxes["BIOMASS_Ecoli_core_w_GAM"] < 1e-8
    if only_ala:
        org_dict = {"ala_dep_model": ala_dep_model}
    else:
        org_dict = {"ala_dep_model": ala_dep_model, "gln_dep_model": gln_dep_model}
    # for model in org_dict.values():
    #     bound_single_model(model)

    compounds_in_medium = {
        "o2_e": 100,
        "h_e": 100,
        "nh4_e": 100,
        "pi_e": 100,
        "glc__D_e": 10,
        "h2o_e": 100,
        "gln__L_e": 0.1,
        "ala__L_e": 0.1,
    }
    system, C_supply = system_from_models(
        org_dict, exc_conc, compounds_in_medium=compounds_in_medium, D=D, system_type=system_type
    )
    return system


def make_ac_gluc_system(system_type="CA", D=0.1):
    exc_conc = 20

    gluc_model = initialize_single_model()
    gluc_model.remove_reactions([gluc_model.reactions.get_by_id("ACt2r")])
    gluc_transporters = get_transport_reactions(gluc_model)
    ac_model = initialize_single_model()
    ac_model.remove_reactions([ac_model.reactions.get_by_id("GLCpts")])
    ac_transporters = get_transport_reactions(ac_model)
    org_dict = {"gluc_model": gluc_model, "ac_model": ac_model}
    # for model in org_dict.values():
    #     bound_single_model(model)

    compounds_in_medium = {
        "o2_e": 100,
        "ac_e": 10,
        "h_e": 100,
        "nh4_e": 100,
        "pi_e": 100,
        # "glu__L_e": 1,
        "glc__D_e": 10,
        "h2o_e": 100,
    }
    system, C_supply = system_from_models(
        org_dict, exc_conc, compounds_in_medium=compounds_in_medium, D=D, system_type=system_type
    )
    return system



if __name__ == "__main__":
    make_mutually_dependent_community(save_models=True)