# ©2022 ETH Zurich, Axel Theorell

import pandas as pd
import numpy as np
from wl_solver import WLInterface, WLParser
from minimal_model import minimal_stoichiometric_model
from utility_classes import Consortium, ChemicalEnvironment
from symbolic_system.specific_systems import (
    ChemostatSystemRA,
    ChemostatSystemRC,
    BatchSystemRC,
)
from symbolic_system.system_evaluator import SystemEvaluator
import matplotlib.pyplot as plt
from collections import defaultdict


def create_min_env(org_names, D=1, C=(2, 2), uptake_prop=2, RC=False, batch=False):
    org_dict = {name: minimal_stoichiometric_model(name) for name in org_names}
    for model in org_dict.values():
        purge_underscore(model)
    consortium = Consortium(org_dict, uptake_prop=uptake_prop)
    # make a chemical environment
    extracellular = {
        met.id
        for met in list(org_dict.values())[0].metabolites
        if met.compartment == "e"
    }
    environment = ChemicalEnvironment(
        D=D, C_supply=pd.Series(C, index=sorted(extracellular))
    )
    chemostatSystem = ChemostatSystemRA(consortium, environment)
    return chemostatSystem, environment, consortium


def purge_underscore(model, replacement=""):

    for met in list(model.metabolites):
        # model.remove_metabolites([met])
        met.id = met.id.replace("_", replacement)
        # model.add_metabolites([met])
    model.repair()
    for r in list(model.reactions):
        model.remove_reactions([r])
        r.id = r.id.replace("_", replacement)
        model.add_reactions([r])
    model.repair()


def gen_result(interface, alpha=0.5, total_conc=5):
    # make stoichiometric models
    org_names = ["Organism1", "Organism2"]
    chemostatSystem, environment, consortium = create_min_env(
        org_names, C=(total_conc * alpha, total_conc * (1 - alpha))
    )
    wl_expr = WLInterface.build_wl_system(chemostatSystem)

    result = interface.solve_wl_system(wl_expr)
    results_dict = WLParser.parse_output(result, separate_solutions=True)
    # tol = 1e-10
    # for key, result_list in results_dict.items():
    #     for result in result_list:
    #         sys_sol = SystemEvaluator.eval(chemostatSystem, result)
    #         for eq_dict in sys_sol['eqs'].values():
    #             for val in eq_dict.values():
    #                 assert np.abs(val) <= tol
    #         for ineq_dict in sys_sol['ineqs'].values():
    #             for val in ineq_dict.values():
    #                 assert val <= tol
    return results_dict


def plot_simulations(joint_result):
    fig, ax = plt.subplots(1, 1)
    keywords = ["regular_solution", "zero_solution_1"]
    endings = [" Organism 1", " Organism 2"]
    labels = list()
    for word in keywords:
        for i in range(2):
            labels.append(word + endings[i])
    colors = ["blue", "green", "cyan", "orange"]
    colors = {word: color for word, color in zip(labels, colors)}
    data_dict = defaultdict(list)
    for key, val in joint_result.items():
        for word in keywords:
            if word in val:
                for i in range(2):
                    p_key = word + endings[i]
                    data_dict[p_key].append((key, val[word][i]))
    for key, array in data_dict.items():
        array = np.array(array)
        ax.plot(
            array[:, 0],
            array[:, 1],
            marker=".",
            label=key.replace("zero_solution_1", "zs").replace(
                "regular_solution", "cs"
            ),
            color=colors[key],
        )
    # legend_lists = np.array([[val, key] for key, val in legend_dict.items()])
    ax.legend()
    plt.show()


def main():
    interface = WLInterface()
    joint_result = defaultdict(dict)
    for i in np.linspace(0, 1, 11):
        results_dict = gen_result(interface, alpha=i, total_conc=10)
        if "regular_solution" in results_dict:
            result = results_dict["regular_solution"]
            assert len(result) == 1
            result = result[0]
            joint_result[i]["regular_solution"] = (
                result["XOrganism1"],
                result["XOrganism2"],
            )
        if "zero_solution_1" in results_dict:
            result = results_dict["zero_solution_1"]
            if len(result) == 2:
                x_1_sols = (result[0]["XOrganism1"], result[1]["XOrganism1"])
                x_2_sols = (result[0]["XOrganism2"], result[1]["XOrganism2"])
            elif len(result) == 1:
                x_1_sols = result[0]["XOrganism1"]
                x_2_sols = result[0]["XOrganism2"]
            joint_result[i]["zero_solution_1"] = (np.max(x_1_sols), np.max(x_2_sols))
    plot_simulations(joint_result)
    interface.session.terminate()


if __name__ == "__main__":
    main()
