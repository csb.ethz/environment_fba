# ©2022 ETH Zurich, Axel Theorell

import os
import pickle
from run_from_shell_script import OUTPUT_PATH
import matplotlib.pyplot as plt
from symbolic_system.naming_utils import NamingUtils
import numpy as np
import json
import pandas as pd
from e_coli_core_ensemble_construction import initialize_single_model, add_ala_depedency, get_transport_reactions
import sys
from matplotlib import colors
plt.rc('font', size=20) #controls default text size
plt.rc('axes', titlesize=20) #fontsize of the title
plt.rc('axes', labelsize=20) #fontsize of the x and y labels
plt.rc('xtick', labelsize=15) #fontsize of the x tick labels
plt.rc('ytick', labelsize=15) #fontsize of the y tick labels
plt.rc('legend', fontsize=20)

THRESHOLD = 1e-8
# X1_NAME = "X_gluc_model"
# X2_NAME = "X_ac_model"
# MU1_NAME = "Mu_gluc_model"
# MU2_NAME = "Mu_ac_model"
# MARKER_SIZE = 2

def tweak_output_string(string):
    string = string.replace("ala__L_e", "alanine").replace("gln__L_e", "glutamine").replace("ala_dep_model", "ala-aux").replace("gln_dep_model", "gln-aux").replace("_", " ")
    return string

def make_surface_plots(infos, output_dir):
    # infos = infos.tolist()
    X1_NAME, X2_NAME = sorted(infos[0]['partial_start'].keys())
    MU1_NAME = NamingUtils.growth_name_from_biomass_name(X1_NAME)
    MU2_NAME = NamingUtils.growth_name_from_biomass_name(X2_NAME)
    fig, axes = plt.subplots(1, 2, figsize=(20, 10))#, subplot_kw={'projection': '3d'})
    axes = axes.reshape(1,2)
    x = np.array([item['x_least_squares'][X1_NAME] for item in infos])
    y = np.array([item['x_least_squares'][X2_NAME] for item in infos])
    z = np.log10(np.array([list(item['violation_least_squares'].values())[0] for item in infos]) + 1e-8)
    order = (-z).argsort()
    ranks = order.argsort()
    sc = axes[0,1].scatter(x[order], y[order], c=z[order], cmap="coolwarm")
    # sc = axes[0, 0].scatter(x, y, c=z)
    fig.colorbar(sc, ax=axes[0,0], label="log residual")
    axes[0, 0].set(xlabel=tweak_output_string(X1_NAME) ,ylabel=tweak_output_string(X2_NAME), title="Residuals MILP")


    x = np.array([item['partial_start'][X1_NAME] for item in infos])
    y = np.array([item['partial_start'][X2_NAME] for item in infos])
    z = np.log10(np.array([list(item['violation_MILP'].values())[0] for item in infos]) + 1e-8)
    sc = axes[0,0].scatter(x[order], y[order], c=z[order], cmap="coolwarm")
    fig.colorbar(sc, ax=axes[0,1], label="log residual")
    axes[0, 1].set(xlabel=tweak_output_string(X1_NAME), ylabel=tweak_output_string(X2_NAME), title="Residuals MILP + LM")

    fig.savefig(
        os.path.join(output_dir, "res_surf.pdf"),
        transparent=False,
        dpi=300,
        bbox_inches="tight",
    )
    plt.close()
    # for the remainder, only focus on the feasible solutions
    # plt.subplots_adjust(wspace=0.4)
    sol_num = len([item for item in infos if list(item['violation_least_squares'].values())[0] < THRESHOLD])
    most_relevant = [item for ind, item in enumerate(infos[np.flip(order)]) if ind < sol_num]
    metabolite_exchange_plot(most_relevant, X1_NAME, X2_NAME, output_dir)
    glucose_plot(most_relevant,X1_NAME, X2_NAME, output_dir)
    concentration_plots(most_relevant,X1_NAME, X2_NAME, output_dir)
    transport_plots(most_relevant, X1_NAME, X2_NAME, output_dir, x_scaled=True)
    transport_plots(most_relevant, X1_NAME, X2_NAME, output_dir)

    make_tables(most_relevant, X1_NAME, X2_NAME, output_dir)

def metabolite_exchange_plot(infos, X1_NAME, X2_NAME, output_dir):
    # now find all transport reactions
    model = initialize_single_model()
    add_ala_depedency(model)
    transport = get_transport_reactions(model)
    model_transport_strings = [r.id for r in transport]
    model_names = [X1_NAME.replace(NamingUtils.BIOMASS_VARIABLE_PREFIX, ""),
                   X2_NAME.replace(NamingUtils.BIOMASS_VARIABLE_PREFIX, "")]
    transport_strings = [el for el in infos[0]['x_least_squares'].index if NamingUtils.remove_model_suffix(el, model_names) in model_transport_strings]
    # make a mapping between transport reactions and transport of specific molecules
    # molecules = sorted(["o2_e", "h_e", "nh4_e", "pi_e", "glc__D_e", "h2o_e", "gln__L_e", "ala__L_e"])
    molecules = sorted(["gln__L_e", "ala__L_e"])
    mapping = pd.DataFrame(0, index=molecules, columns=sorted(transport_strings))
    for r_id in transport_strings:
        r = model.reactions.get_by_id(NamingUtils.remove_model_suffix(r_id, model_names))
        for m, val in r.metabolites.items():
            if m.id in molecules:
                mapping.loc[m.id, r_id] += val
    # make all the z vectors
    z_dict = dict()
    for model_name in model_names:
        for molecule in molecules:
            z = np.zeros(len(infos))
            for transport_string, val in mapping.loc[molecule, :].iteritems():
                if transport_string.endswith(model_name):
                    z += val * np.array([item['x_least_squares'][transport_string] for item in infos])
            z_dict[molecule + "_" + model_name] = z

    # size = int(np.ceil(np.sqrt(len(z_dict))))
    fig, axes = plt.subplots(1, len(z_dict), figsize=(5*len(z_dict), 4))#, subplot_kw={'projection': '3d'})
    plt.subplots_adjust(wspace=0.45)
    for molecule, ax in zip(z_dict, axes.flatten()):

        x = np.array([item['x_least_squares'][X1_NAME] for item in infos])
        y = np.array([item['x_least_squares'][X2_NAME] for item in infos])
        z = z_dict[molecule]
        if model_names[0] in molecule:
            z = z * x
        elif model_names[1] in molecule:
            z = z * y
        values = [np.minimum(np.min(z),-1e-10), 0., np.maximum(np.max(z), 1e-10)]
        divnorm = colors.DivergingNorm(vmin=values[0], vcenter=values[1], vmax=values[2])
        sc = ax.scatter(x, y, c=z, norm=divnorm, cmap="coolwarm")
        fig.colorbar(sc, ax=ax, label="flux")
        ax.set(xlabel=tweak_output_string(X1_NAME), ylabel=tweak_output_string(X2_NAME), title=tweak_output_string(molecule))

    file = os.path.join(output_dir, "metabolite_exchange_plots.pdf")
    fig.tight_layout()
    fig.savefig(
        file,
        transparent=False,
        dpi=300,
        bbox_inches="tight",
    )
    plt.close()

def glucose_plot(infos, X1_NAME, X2_NAME, output_dir):
    model_names = [X1_NAME.replace(NamingUtils.BIOMASS_VARIABLE_PREFIX, ""),
                   X2_NAME.replace(NamingUtils.BIOMASS_VARIABLE_PREFIX, "")]
    size = int(1)

    fig, ax = plt.subplots(size, size, figsize=(5*size, 5*size))#, subplot_kw={'projection': '3d'})
    transport_string = "glc__D_e"

    x = np.array([item['x_least_squares'][X1_NAME] for item in infos])
    y = np.array([item['x_least_squares'][X2_NAME] for item in infos])
    z = np.array([item['x_least_squares'][transport_string] for item in infos])
    values = [np.min(z), 3., 5]
    divnorm = colors.DivergingNorm(vmin=values[0], vcenter=values[1], vmax=values[2])
    sc = ax.scatter(x, y, c=z, cmap="coolwarm", norm = divnorm)
    fig.colorbar(sc, ax=ax, label="concentration")
    ax.set(xlabel=tweak_output_string(X1_NAME), ylabel=tweak_output_string(X2_NAME), title="extracellular glucose")

    file = os.path.join(output_dir, "glucose_plot.pdf")
    fig.savefig(
        file,
        transparent=False,
        dpi=300,
        bbox_inches="tight",
    )
    plt.close()

def concentration_plots(infos, X1_NAME, X2_NAME, output_dir):
    model_names = [X1_NAME.replace(NamingUtils.BIOMASS_VARIABLE_PREFIX, ""),
                   X2_NAME.replace(NamingUtils.BIOMASS_VARIABLE_PREFIX, "")]
    concentration_strings = [el for el in infos[0]['x_least_squares'].index if
                         (model_names[0] not in el) and (model_names[1] not in el)]
    size = int(np.ceil(np.sqrt(len(concentration_strings))))
    fig, axes = plt.subplots(size, size, figsize=(5*size, 5*size))#, subplot_kw={'projection': '3d'})
    for transport_string, ax in zip(concentration_strings, axes.flatten()):

        x = np.array([item['x_least_squares'][X1_NAME] for item in infos])
        y = np.array([item['x_least_squares'][X2_NAME] for item in infos])
        z = np.array([item['x_least_squares'][transport_string] for item in infos])
        sc = ax.scatter(x, y, c=z, cmap="coolwarm")
        fig.colorbar(sc, ax=ax)
        ax.set(xlabel=tweak_output_string(X1_NAME), ylabel=tweak_output_string(X2_NAME), title=tweak_output_string(transport_string))

    file = os.path.join(output_dir, "concentration_plots.pdf")
    fig.savefig(
        file,
        transparent=False,
        dpi=300,
        bbox_inches="tight",
    )
    plt.close()

def transport_plots(infos, X1_NAME, X2_NAME, output_dir, x_scaled=False):
    # now find all transport reactions
    model = initialize_single_model()
    add_ala_depedency(model)
    transport = get_transport_reactions(model)
    model_transport_strings = [r.id for r in transport]
    model_names = [X1_NAME.replace(NamingUtils.BIOMASS_VARIABLE_PREFIX, ""),
                   X2_NAME.replace(NamingUtils.BIOMASS_VARIABLE_PREFIX, "")]
    transport_strings = [el for el in infos[0]['x_least_squares'].index if NamingUtils.remove_model_suffix(el, model_names) in model_transport_strings]
    size = int(np.ceil(np.sqrt(len(transport_strings))))
    fig, axes = plt.subplots(size, size, figsize=(5*size, 5*size))#, subplot_kw={'projection': '3d'})
    for transport_string, ax in zip(transport_strings, axes.flatten()):

        x = np.array([item['x_least_squares'][X1_NAME] for item in infos])
        y = np.array([item['x_least_squares'][X2_NAME] for item in infos])
        z = np.array([item['x_least_squares'][transport_string] for item in infos])
        if x_scaled:
            if model_names[0] in transport_string:
                z = z * x
            elif model_names[1] in transport_string:
                z = z * y
        sc = ax.scatter(x, y, c=z, cmap="coolwarm")
        fig.colorbar(sc, ax=ax)
        ax.set(xlabel=tweak_output_string(X1_NAME), ylabel=tweak_output_string(X2_NAME), title=tweak_output_string(transport_string))
    if x_scaled:
        file = os.path.join(output_dir, "fluxes_plots_x_scaled.pdf")
    else:
        file = os.path.join(output_dir, "fluxes_plots.pdf")
    fig.savefig(
        file,
        transparent=False,
        dpi=300,
        bbox_inches="tight",
    )
    plt.close()



def make_tables(infos, X1_NAME, X2_NAME, output_dir):
    # now print the best solutions to file
    # also make csv output that hopefully is easier to read
    most_relevant = infos

    for item in most_relevant:
        item['x_least_squares'] = item['x_least_squares'].iloc[[not el.startswith(NamingUtils.LAGRANGE_NAME) for el in item['x_least_squares'].index]]
    # also make csv output that hopefully is easier to read

    flux_index = sorted([item for item in most_relevant[0]['x_least_squares'].index])  # {flux.replace(model_names[0], "") for flux in most_relevant[0]['x_least_squares'].index}
    flux_index.insert(0, flux_index.pop(flux_index.index(X2_NAME)))
    flux_index.insert(0, flux_index.pop(flux_index.index(X1_NAME)))
    table_version = pd.DataFrame(0, index=flux_index, columns=range(len(most_relevant)))

    for ind, item in enumerate(most_relevant):
        table_version.loc[:, ind] = item['x_least_squares']
        item['x_least_squares'] = [[item['x_least_squares'].index[i],item['x_least_squares'][i]] for i in range(len(item['x_least_squares']))]

        item.pop('substituted_least_squares')
    table_version = table_version.round(3)
    # turn series into nicely readable tuples
    table_version.sort_values(by = [X2_NAME, X1_NAME], axis = 1, inplace=True)
    with open(os.path.join(output_dir, "flux_distributions.json"), "w+", encoding="utf-8") as file:
        json.dump(most_relevant, file)
    table_version.to_csv(os.path.join(output_dir, "flux_distributions_table.csv"))

def read_infos(output_dir):
    infos = list()
    for filename in os.listdir(output_dir):
        if filename.endswith(".pkl"):
            file_path = os.path.join(output_dir, filename)

            with open(file_path, 'rb') as f:
                infos.append(pickle.load(f))
    return infos

def main(output_dir):

    # read in all the files

    # output_dir = os.path.join(OUTPUT_PATH, "Feb-10-2022_coli_problem_efe4042_CA")
    # output_dir = os.path.join(OUTPUT_PATH, "Feb-09-2022_coli_problem_683c436_CA")
    infos = read_infos(output_dir)
    infos = np.array(infos)
    make_surface_plots(infos, output_dir)
    # print(x_s)

if __name__ == '__main__':
    if len(sys.argv) == 2:
        output_dir = os.path.join(OUTPUT_PATH, sys.argv[1])
        main(output_dir)
    else:
        raise IOError("An input directory is required")
