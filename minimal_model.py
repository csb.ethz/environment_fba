# ©2022 ETH Zurich, Axel Theorell

import cobra
import numpy as np


def minimal_stoichiometric_model(name="organism"):

    met_names = ["A_e", "B_e", "A_c", "B_c"]
    metabolites = dict()
    for met in met_names:
        if met.endswith("e"):
            metabolites[met] = cobra.Metabolite(met, compartment="e")
        else:
            metabolites[met] = cobra.Metabolite(met, compartment="c")
    r_names = ["vA_" + name, "vB_" + name, "Mu_" + name]
    reactions = {name: cobra.Reaction(name) for name in r_names}
    for reaction in reactions.values():
        reaction.lower_bound = 0
    reactions["vA_" + name].upper_bound = 2
    reactions["vB_" + name].upper_bound = 2
    reactions["vA_" + name].add_metabolites(
        {metabolites["A_e"]: -1, metabolites["A_c"]: 1}
    )
    reactions["vB_" + name].add_metabolites(
        {metabolites["B_e"]: -1, metabolites["B_c"]: 1}
    )
    if name == "Organism1":
        reactions["Mu_" + name].add_metabolites(
            {metabolites["A_c"]: -2, metabolites["B_c"]: -1}
        )
    else:
        reactions["Mu_" + name].add_metabolites(
            {metabolites["A_c"]: -1, metabolites["B_c"]: -2}
        )

    model = cobra.Model(name)
    model.add_metabolites(metabolites.values())
    model.add_reactions(reactions.values())
    for r in list(model.reactions):
        if r.upper_bound == 1000:
            r.upper_bound = np.inf
    model.repair()
    return model


def minimal_crossfeeding():
    org_dict = dict()
    name = "Organism1"
    model = cobra.Model(name)
    met_names = ["Ae", "Ac", "Be", "Pc"]
    metabolites = dict()
    for met in met_names:
        if met.endswith("e"):
            metabolites[met] = cobra.Metabolite(met, compartment="e")
        else:
            metabolites[met] = cobra.Metabolite(met, compartment="c")
    r_names = ["EXA", "tA" + name, "vP" + name, "vPB" + name, "Mu_" + name]
    reactions = {name: cobra.Reaction(name) for name in r_names}
    for reaction in reactions.values():
        reaction.lower_bound = 0
    reactions["tA" + name].upper_bound = 1
    reactions["vP" + name].add_metabolites(
        {metabolites["Ac"]: -3, metabolites["Pc"]: 2}
    )
    reactions["vPB" + name].add_metabolites(
        {metabolites["Ac"]: -3, metabolites["Pc"]: 1, metabolites["Be"]: 2}
    )
    reactions["Mu_" + name].add_metabolites({metabolites["Pc"]: -1})
    reactions["tA" + name].add_metabolites(
        {metabolites["Ae"]: -1, metabolites["Ac"]: 1}
    )
    reactions["EXA"].add_metabolites({metabolites["Ae"]: 1})
    model.add_metabolites(metabolites.values())
    model.add_reactions(reactions.values())
    for r in list(model.reactions):
        if r.upper_bound == 1000:
            r.upper_bound = np.inf
    model.repair()
    model.objective = "Mu_" + name
    model.repair()
    res1 = model.optimize()
    # remove the test reaction from
    model.remove_reactions(["EXA"])
    model.repair()
    org_dict[name] = model

    # next model is simpler
    name = "Organism2"
    model = cobra.Model(name)
    met_names = ["Be", "Bc", "Ae"]
    metabolites = dict()
    for met in met_names:
        if met.endswith("e"):
            metabolites[met] = cobra.Metabolite(met, compartment="e")
        else:
            metabolites[met] = cobra.Metabolite(met, compartment="c")
    r_names = ["EXB", "EXA", "tB" + name, "Mu_" + name]
    reactions = {name: cobra.Reaction(name) for name in r_names}
    for reaction in reactions.values():
        reaction.lower_bound = 0
    reactions["tB" + name].upper_bound = 1
    reactions["tB" + name].add_metabolites(
        {metabolites["Be"]: -1, metabolites["Bc"]: 1}
    )
    reactions["Mu_" + name].add_metabolites(
        {metabolites["Bc"]: -2, metabolites["Ae"]: 1}
    )
    reactions["EXB"].add_metabolites({metabolites["Be"]: 1})
    reactions["EXA"].add_metabolites({metabolites["Ae"]: -1})
    model.add_metabolites(metabolites.values())
    model.add_reactions(reactions.values())
    for r in list(model.reactions):
        if r.upper_bound == 1000:
            r.upper_bound = np.inf
    model.repair()
    model.objective = "Mu_" + name
    model.repair()
    res2 = model.optimize()
    # remove the test reaction from
    model.remove_reactions(["EXB", "EXA"])
    model.repair()
    org_dict[name] = model
    return org_dict
