# ©2022 ETH Zurich, Axel Theorell

from test import Tester
from numeric_solution.gurobi_interface import GurobiInterface
import numpy as np
from symbolic_system.derivatives import SystemDerivatives
from symbolic_system.system_evaluator import SystemEvaluator
from symbolic_system.naming_utils import NamingUtils
from numeric_solution.scipy_root_solver import least_squares_solve
import pandas as pd
import sys
from pathlib import Path
from datetime import date
import os
import pickle
from e_coli_core_ensemble_construction import make_mutually_dependent_community
import subprocess

OUTPUT_PATH = "output"

def make_test_system():
    org_names = ["Organism1", "Organism2"]
    system, environment, consortium = Tester.create_min_env(org_names)
    return system


def run_grid(grid_points=2500, grid_max=1, current_point=None, system_type="CA"):
    # system type
    assert (system_type == "CA" or system_type == "CC")
    # make output directory
    commit = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).decode('ascii').strip()
    date_str = date.today().strftime("%b-%d-%Y")
    output_dir = os.path.join("output", date_str + "_coli_problem_" + commit + "_" + system_type)
    Path(output_dir).mkdir(parents=True, exist_ok=True)

    # setup (any) system
    # system = make_ac_gluc_system(system_type=system_type)
    system = make_mutually_dependent_community(system_type=system_type)
    settings = {
        "PoolSearchMode": 0,
        "PoolSolutions": 1,
        "FeasibilityTol": 1e-8,
        "TimeLimit": 2000,
        "OptimalityTol": 1e-5,
        "IntFeasTol": 1e-8,
        "Threads": 1,
    }
    #make the grid points definite in gurobi
    force_fluxes = True
    # make the gurobi model
    model = GurobiInterface.construct_gurobi_model(system, settings=settings)
    # make sure that the model is feasible for all X
    GurobiInterface.relax_eqs(model)
    vars = model.getVars()
    # the number of side points in the grid must be an integer
    assert np.sqrt(grid_points) % 1 == 0

    space = np.linspace(0, grid_max, int(np.sqrt(grid_points)))
    grid = np.meshgrid(space, space)
    derivatives = SystemDerivatives(system)
    # find biomass variable names
    var_names = sorted([key for key in system.sym_dict if key.startswith(NamingUtils.BIOMASS_VARIABLE_PREFIX)])
    for i in range(grid[0].size):
        if current_point is not None:
            if i != current_point:
                continue
        partial_start = {
            var_names[0]: grid[0].flatten()[i],
            var_names[1]: grid[1].flatten()[i],
        }
        print(partial_start)
        # partial_start.update(rel_start)
        GurobiInterface.set_starting_point(model, partial_start, force_fluxes)
        model.optimize()
        subs = {
            system.sym_dict[var.VarName]: var.X
            for var in vars
            if var.VarName in system.sym_dict
        }
        substituted_MILP = SystemEvaluator.eval(system, subs)
        violation_MILP = SystemEvaluator.max_violation(substituted_MILP)

        res = least_squares_solve(system, vars, derivatives)
        x_least_squares = pd.Series(res.x, index=sorted(system.sym_dict.keys()))
        subs = {
            system.sym_dict[key]: val for key, val in x_least_squares.iteritems()
        }
        substituted_least_squares = SystemEvaluator.eval(system, subs)
        violation_least_squares = SystemEvaluator.max_violation(substituted_least_squares)
        file_path = os.path.join(output_dir, "run_" + str(i) + "_.pkl")
        # info_list = [x_least_squares, partial_start, violation_MILP, violation_least_squares, substituted_least_squares]
        info_dict = {"x_least_squares": x_least_squares, "partial_start": partial_start, "violation_MILP": violation_MILP, "violation_least_squares": violation_least_squares, "substituted_least_squares": substituted_least_squares}
        # for item in info_list:
        #     name = [i for i, a in locals().items() if a is item][0]
        #     info_dict[name] = item
        with open(file_path, "wb") as f:
            pickle.dump(info_dict, f)



if __name__ == '__main__':
    if len(sys.argv) == 1:
        run_grid()
    elif len(sys.argv) == 4:
        run_grid(grid_points=int(sys.argv[1]), grid_max=int(sys.argv[2]),
                 system_type=(sys.argv[3]))
    elif len(sys.argv) == 5:
        run_grid(grid_points=int(sys.argv[1]), grid_max=int(sys.argv[2]), current_point=int(sys.argv[3]), system_type=(sys.argv[4]))
    else:
        raise IOError("Either 4, 3 or no extra input arguments")